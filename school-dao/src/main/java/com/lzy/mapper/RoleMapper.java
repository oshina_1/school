package com.lzy.mapper;

import com.lzy.pojo.Role;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface RoleMapper {
    @Select("SELECT * FROM `role` r LEFT JOIN user_role ur ON r.Id = ur.ROLE_ID " +
            "WHERE USER_ID = #{id}")
    List<Role> findAllByUserId(long id);
}
