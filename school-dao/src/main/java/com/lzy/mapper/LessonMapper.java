package com.lzy.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lzy.pojo.Lesson;
import org.apache.ibatis.annotations.Mapper;

/**
* @author Administrator
* @description 针对表【lesson(课程表)】的数据库操作Mapper
* @createDate 2023-09-01 17:31:31
* @Entity com.lzy.pojo.Lesson
*/
@Mapper
public interface LessonMapper extends BaseMapper<Lesson> {

}




