package com.lzy.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.lzy.pojo.Subject;
import com.lzy.vo.SubjectVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
* @author Administrator
* @description 针对表【subject(学科表)】的数据库操作Mapper
* @createDate 2023-09-01 13:55:06
* @Entity com.lzy.pojo.Subject
*/
@Mapper
public interface SubjectMapper extends BaseMapper<Subject> {

    List<Subject> selectSubjectVo(IPage<Subject> page, String name);

    //根据课程id找到对应课程的评价信息
    List<SubjectVo> selectSubjectByUserId(Long facultiesId, Long userId);


    Integer confirmSelectSubject(@Param("subject") Subject subject);

}




