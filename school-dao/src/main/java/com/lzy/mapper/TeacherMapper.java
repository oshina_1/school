package com.lzy.mapper;

import com.lzy.pojo.Classroom;
import com.lzy.pojo.Faculties;
import com.lzy.vo.*;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface TeacherMapper {
    int findCount(@Param("classroom") ClassroomVo classroom);

    List<ClassroomVo> findByPage(ClassroomPageVo classroomVo);

    List<Faculties> selectFac();

    List<StudentVo> selectStuBySub(StuPageVo stuPageVo);

    int selectCountBySub(StudentVo studentVo);

    int selectStuCountByClassId(StuScoreVo stuScoreVo);

    List<StuScoreVo> selectStuScorePageByClassId(StuScorePageVo stuScorePageVo);
}
