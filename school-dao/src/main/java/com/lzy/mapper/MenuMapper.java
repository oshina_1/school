package com.lzy.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lzy.pojo.Menu;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface MenuMapper extends BaseMapper<Menu> {

    @Select("SELECT * FROM menu m " +
            "LEFT JOIN role_menu rm ON m.menu_id = rm.MENU_ID " +
            "LEFT JOIN user_role ur ON ur.ROLE_ID = rm.ROLE_ID " +
            "WHERE ur.USER_id = #{id} AND MENU_TYPE in ('M', 'C')")
    List<Menu> findMenuByUserId(long id);

    @Select("SELECT * FROM menu m " +
            "LEFT JOIN role_menu rm ON m.menu_id = rm.MENU_ID " +
            "LEFT JOIN user_role ur ON ur.ROLE_ID = rm.ROLE_ID " +
            "WHERE ur.USER_id = #{id}")
    List<Menu> findAllByUserId(long id);
}
