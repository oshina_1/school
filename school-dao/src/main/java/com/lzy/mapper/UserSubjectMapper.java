package com.lzy.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lzy.pojo.UserSubject;
import com.lzy.vo.UserSubjectVo;

import java.util.List;

/**
 * @author Administrator
 * @description 针对表【user_subject(学生选课表)】的数据库操作Mapper
 * @createDate 2023-09-02 11:05:54
 * @Entity com.lzy.pojo.UserSubject
 */
public interface UserSubjectMapper extends BaseMapper<UserSubject> {

    List<UserSubjectVo> teaSubject(Long teaId);

    //根据学生的id找到对应的课程
    List<UserSubjectVo> stuSubject(Long userId);

    void deleteByIds(List<Long> ids
    );


}




