package com.lzy.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.lzy.pojo.Score;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lzy.vo.ScoreInfoVo;
import com.lzy.vo.ScoreVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
* @author Administrator
* @description 针对表【score(分数表)】的数据库操作Mapper
* @createDate 2023-09-14 11:04:54
* @Entity com.lzy.pojo.Score
*/
public interface ScoreMapper extends BaseMapper<Score> {

    List<ScoreVo> selectScoreByUserIdAndSemester(IPage<ScoreVo> page, @Param("userId") Long userId, @Param("grateId") Long grateId);
    List<ScoreInfoVo> selectScoreByFacAndUserId(@Param("subjectId") Long subjectId, @Param("facultiesId") Long facultiesId);
}




