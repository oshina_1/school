package com.lzy.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lzy.pojo.Classroom;
import org.apache.ibatis.annotations.Mapper;


/**
* @author 24846
* @description 针对表【classroom】的数据库操作Mapper
* @createDate 2023-09-01 14:44:35
* @Entity com.ekgc.pojo.Classroom
*/
@Mapper
public interface ClassroomMapper extends BaseMapper<Classroom> {

}




