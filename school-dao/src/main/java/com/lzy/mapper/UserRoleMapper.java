package com.lzy.mapper;

import com.ekgc.demo.pojo.UserRole;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
* @author 24846
* @description 针对表【user_role(用户角色表)】的数据库操作Mapper
* @createDate 2023-09-01 14:44:35
* @Entity com.ekgc.pojo.UserRole
*/
public interface UserRoleMapper extends BaseMapper<UserRole> {
    int insertAll(Long userId,Long roleId);

}




