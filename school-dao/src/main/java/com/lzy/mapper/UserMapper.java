package com.lzy.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lzy.pojo.User;
import com.lzy.vo.StudentVo;
import com.lzy.vo.SubjectVo;
import com.lzy.vo.UpdateUserVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import java.util.List;

/**
* @author zyj
* @description 针对表【user】的数据库操作Mapper
* @createDate 2023-08-29 15:54:15
* @Entity com.example.pojo.User
*/
@Mapper
public interface UserMapper extends BaseMapper<User>{

    @Select("select name from user where id = #{id}")
    String findName(Long id);
    //新增用户
    int insertAll(User user);
    //查询用户
    List<User> selectUser(User user);
    //删除用户
    boolean update(UpdateUserVo updateUserVo);

    Long findFacultiesId(Long id);

    StudentVo findById(Long stuId);

    List<User> findAllTeachers();


    List<SubjectVo> showSelectSubjects(Long id);
    //修改用户

}




