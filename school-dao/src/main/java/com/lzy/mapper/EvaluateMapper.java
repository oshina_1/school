package com.lzy.mapper;

import com.lzy.pojo.Evaluate;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lzy.vo.EvaluateVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
* @author Administrator
* @description 针对表【evaluate(教师评价)】的数据库操作Mapper
* @createDate 2023-09-07 15:14:21
* @Entity com.lzy.pojo.Evaluate
*/
@Mapper
public interface EvaluateMapper extends BaseMapper<Evaluate> {
    List<EvaluateVo> findTeaEvaBySubjectId(@Param("subjectName") String subjectName,@Param("roleId") Long roleId,
                                           @Param("teacherName") String teacherName, @Param("facultiesId") Long facultiesId);

    List<EvaluateVo> findEvaBySubjectId(@Param("subjectId") Long subjectId,@Param("roleId") Long roleId,
                                        @Param("facultiesId") Long facultiesId, @Param("userId") Long userId,
                                        @Param("createBy") Long createBy);
}




