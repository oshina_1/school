package com.lzy.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.lzy.pojo.AuditDetail;
import com.lzy.vo.AuditDetailVo;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface AuditDetailMapper extends BaseMapper<AuditDetail> {


    @Update("update audit_detail set audit_status = #{status} where id = #{id}")
    void updateByApplyId(@Param("id") Long id, @Param("status") int status);

    @Update("UPDATE audit_detail SET audit_time = #{auditTime} WHERE id = #{id}")
    void updateAuditTime(@Param("id") Long id, @Param("auditTime") LocalDateTime auditTime);

    @Update("UPDATE audit_detail SET remark = #{remark} WHERE id = #{id}")
    void updateRemark(@Param("id") Long id, @Param("remark") String remark);

//    @Select("SELECT ad.*, u.name \n" +
//            "FROM audit_detail ad \n" +
//            "JOIN user u ON ad.auditor_id = u.id \n" +
//            "WHERE ad.auditor_id = #{auditorId}")
@Select("SELECT d.*, u.name auditname,u2.name applyname, a.user_id, a.apply_type, a.title FROM audit_detail d\n" +
        "LEFT JOIN audit_apply a ON a.id = d.apply_id\n" +
        "LEFT JOIN `user` u ON u.id = d.auditor_id\n" +
        "LEFT JOIN `user` u2 ON u2.id = a.user_id\n" +
        "WHERE d.auditor_id = #{auditorId} AND d.audit_status IN(1,-1)")
    List<AuditDetailVo> findDetailByAuditorId(IPage<AuditDetailVo> page, Long auditorId);


@Select("select count(*) from audit_detail where auditor_id = #{userId} and audit_status = 0")
    int auditNotice(long userId);
}
