package com.lzy.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lzy.pojo.Attendance;
import com.lzy.vo.AttendanceVo;
import com.lzy.vo.StatsVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
* @author Administrator
* @description 针对表【attendance(签到表)】的数据库操作Mapper
* @createDate 2023-08-31 13:40:08
* @Entity com.lzy.pojo.Attendance
*/
@Mapper
public interface AttendanceMapper extends BaseMapper<Attendance> {


    List<AttendanceVo> allStudentByTeacherId(Long userId);

    //根据三个参数来确认签到人数
//    int queryStuAttend(@Param("status") Integer status,
//            @Param("startTime") Date startTime,@Param("subjectId") Long subjectId);

    List<AttendanceVo> queryStuAttendInfo(@Param("status") Integer status,@Param("facultiesId") Long facultiesId,
                                      @Param("startTime") Date startTime, @Param("subjectId") Long subjectId);




}




