package com.lzy.mapper;

import com.lzy.pojo.User;
import com.lzy.vo.*;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface TeaMapper {

    int findCount(StuScoreVo stuScoreVo);

    List<StuScoreVo> findByPage(StuScorePageVo stuScorePageVo);

    int findExaminationCount(ExaminationVo examinationVo);

    List<ExaminationVo> findExamination(ExaminationPageVo examinationPageVo);

    List<ExaminationVo> selectPaper(Long facultiesId);

    List<ExaminationVo> selectClassroom();

    List<ExaminationVo> selectSubject(Long facultiesId);

    int deleteInv(Long id);

    List<ExaminationVo> selectLead(Long facultiesId);

    List<ExaminationVo> selectAssociate(Long facultiesId);

    int updateInv(ExaminationVo examinationVo);

    int insertInv(ExaminationVo examinationVo);

    int stuCount(Long pId);

    int findPaperCount(PaperVo paperVo);

    List<PaperVo> findPaper(PaperPageVo paperPageVo);

    int deletePaper(Long id);

    int insertPaper(PaperVo paperVo);

    int updatePaper(PaperVo paperVo);

    int findClassroomCount(ClassroomVo classroom);

    List<ClassroomVo> findClassroom(ClassroomPageVo classroomPageVo);

    int findUserCount(UserTVo user);

    List<User> selectUser(UserPageVo userPageVo);
}
