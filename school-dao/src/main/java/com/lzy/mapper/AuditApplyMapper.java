package com.lzy.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.lzy.pojo.AuditApply;
import com.lzy.vo.AuditApplyVo;
import com.lzy.vo.AuditDetailVo;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AuditApplyMapper extends BaseMapper<AuditApply> {
    @Select(""
            + "select a.id, a.title, u.name, a.apply_time, a.apply_status, a.apply_type "
            + "from audit_apply a "
            + "left join audit_detail d on a.id = d.apply_id "
            + "left join user u on u.id = a.user_id "
            + "where d.auditor_id = #{userId} and d.audit_status = 0 "
            + "")
    List<AuditApply> findAuditList(long userId);

    @Update(""
            + "update audit_apply "
            + "set apply_status = #{status} "
            + "where id = #{id}"
            + "")
    int updateStatusByApplyId(@Param("id") Long id, @Param("status") int status);

    @Select("SELECT a.title, a.apply_type, apply_time, a.apply_status, u2.name applyname, u.name auditname, d.remark, d.audit_time FROM audit_apply a\n" +
            "LEFT JOIN `user` u2 ON u2.id =a.user_id\n" +
            "LEFT JOIN audit_detail d ON a.id = d.apply_id\n" +
            "LEFT JOIN `user` u ON u.id = d.auditor_id\n" +
            "WHERE a.user_id =#{userId} AND a.apply_status IN(1,-1)")
    List<AuditApplyVo> degree(IPage<AuditApplyVo> page, long userId);

    @Select("SELECT a.title, a.apply_type, apply_time, a.apply_status, u2.name applyname, u.name auditname, d.remark, d.audit_time, a.id FROM audit_apply a\n" +
            "LEFT JOIN `user` u2 ON u2.id =a.user_id\n" +
            "LEFT JOIN audit_detail d ON a.id = d.apply_id\n" +
            "LEFT JOIN `user` u ON u.id = d.auditor_id\n" +
            "WHERE a.user_id =#{userId} AND a.apply_status = 0")
    List<AuditApplyVo> degreeTwo(Long userId);

    @Delete("DELETE audit_apply, audit_detail\n" +
            "FROM audit_apply\n" +
            "INNER JOIN audit_detail ON audit_apply.id = audit_detail.apply_id\n" +
            "WHERE audit_apply.id = #{id}")
    boolean cancel(long id);

//    @Select("select  count(*) from audit_apply where user_id = #{userId} and apply_status = 0")
//    int applyNotice(long userId);

}
