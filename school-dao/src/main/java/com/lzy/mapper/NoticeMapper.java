package com.lzy.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lzy.pojo.Notice;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;


/**
* @author 24846
* @description 针对表【notice(通知公告表)】的数据库操作Mapper
* @createDate 2023-09-01 14:44:35
* @Entity com.ekgc.pojo.Notice
*/
@Mapper
public interface NoticeMapper extends BaseMapper<Notice> {

    List<Notice> findNotice();

    List<Notice> findNotify();
}




