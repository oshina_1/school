package com.lzy.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lzy.pojo.AuditDiagram;
import com.lzy.pojo.User;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AuditDiagramMapper extends BaseMapper<AuditDiagram> {

    @Select("SELECT u.id, u.name FROM USER u \n" +
            "JOIN user_role ur ON ur.user_id = u.id \n" +
            "WHERE ur.role_id = 4")
    List<User> selectTea();
}
