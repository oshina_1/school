package com.lzy.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lzy.pojo.Lectures;
import com.lzy.vo.LecturesVo;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
* @author Administrator
* @description 针对表【lectures(听课表)】的数据库操作Mapper
* @createDate 2023-09-02 11:29:34
* @Entity com.lzy.pojo.Lectures
*/
@Mapper
public interface LecturesMapper extends BaseMapper<Lectures> {

    List<LecturesVo> myLectures(Long userId);
}




