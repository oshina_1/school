package com.lzy.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lzy.pojo.Faculties;
import org.apache.ibatis.annotations.Mapper;

/**
* @author zyj
* @description 针对表【faculties(院系表)】的数据库操作Mapper
* @createDate 2023-09-12 13:50:27
* @Entity generator.domain.Faculties
*/
@Mapper
public interface FacultiesMapper extends BaseMapper<Faculties> {

}




