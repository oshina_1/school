package com.lzy.util;

import lombok.*;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class PageUtils {
    //查询条件
    private String query;
    //当前页
    private int current;
    //数据量
    private int size;
}
