package com.lzy.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 学科表
 * @TableName subject
 */
@TableName(value ="subject")
@Data
public class Subject implements Serializable {
    /**
     * 课程id
     */
    @TableId(type = IdType.AUTO)
    private Long id;

    /**
     * 课程名
     */
    private String name;

    /**
     * 年级id
     */
    private Long grateId;

    /**
     * 课时
     */
    private Integer classHour;

    /**
     * 学分
     */
    private Double credits;

    /**
     * 课程状态  -1:未开始  0:开始  1:结束
     */
    private Integer status;

    /**
     * 课程类型（0：选修 1：必修）
     */
    private Integer type;

    /**
     * 选课人数
     */
    private Integer studentNum;

    /**
     * 创建人
     */
    private Long createBy;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 修改人
     */
    private Long modifyBy;

    /**
     * 修改时间
     */
    private Date modifyTime;


}
