package com.lzy.pojo;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@TableName("audit_detail")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class AuditDetail {

    private long id;
    private long diagramId;
    private long nodeId;
    private long applyId;
    private long auditorId;
    private String remark;
    private LocalDateTime auditTime;
    private LocalDateTime createTime;
    private long auditStatus;

    private String name;

}
