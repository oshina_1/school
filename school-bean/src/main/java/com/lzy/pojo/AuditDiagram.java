package com.lzy.pojo;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@TableName(value = "audit_diagram")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class AuditDiagram {
    private long id;
    private String auditName;
    //  @TableField(typeHandler = FastjsonTypeHandler.class)
    private String auditNodes;
    private long auditType;
    private long auditTimes;
}