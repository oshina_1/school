package com.lzy.pojo;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 
 * @TableName classroom
 */
@TableName(value ="classroom")
@Data
public class Classroom implements Serializable {
    /**
     * 教室ID
     */
    @TableId(type = IdType.AUTO)
    private Long id;
    /**
     * 教室名
     */
    private String name;
    /**
     * 类型
     */
    private String type;
    /**
     * 状态（0已使用  1未使用）
     */
    private Integer status;

    private Long facultiesId;

    private String position;

    /**
     * 创建人
     */
    private String createBy;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 修改人
     */
    private String modifyBy;
    /**
     * 修改时间
     */
    private Date modifyTime;
    @TableLogic
    private Integer deleteId;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}