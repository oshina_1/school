package com.lzy.pojo;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

import static com.baomidou.mybatisplus.annotation.IdType.AUTO;

@TableName("audit_apply")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class AuditApply{

    //指定主键 并自增
    @TableId(type = AUTO)
    private long id;
    private String title;
    private int applyType;
    private long userId;
    private LocalDateTime applyTime;
    private long applyStatus;

    @TableField(exist = false)
    private String name;
}
