package com.lzy.pojo;

import com.baomidou.mybatisplus.annotation.TableId;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Menu {

  @TableId
  private long menuId;
  private String menuName;
  private long parentId;
  private long orderNum;
  private String path;
  private String component;
  private String query;
  private long isFrame;
  private long isCache;
  private String menuType;
  private String visible;
  private String status;
  private String perms;
  private String icon;
  private long create;
  private LocalDateTime createTime;
  private long modify;
  private LocalDateTime modifyTime;
  private String remark;

  // 子权限
  private List<Menu> children;

}
