package com.lzy.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 院系表
 * @TableName faculties
 */
@TableName(value ="faculties")
@Data
public class Faculties implements Serializable {
    /**
     * 院系id
     */
    @TableId(value = "ID", type = IdType.AUTO)
    private Long id;

    /**
     * 院系名
     */
    @TableField(value = "NAME")
    private String name;

    /**
     * 创建人
     */
    @TableField(value = "CREATE_BY")
    private Long createBy;

    /**
     * 创建时间
     */
    @TableField(value = "CREATE_TIME")
    private LocalDateTime createTime;

    /**
     * 修改人
     */
    @TableField(value = "MODIFY_BY")
    private Long modifyBy;

    /**
     * 修改时间
     */
    @TableField(value = "MODIFY_TIME")
    private LocalDateTime modifyTime;

    /**
     *
     */
    @TableField(value = "remark")
    private String remark;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}
