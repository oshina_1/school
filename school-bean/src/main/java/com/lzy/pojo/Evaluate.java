package com.lzy.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 教师评价
 * @TableName evaluate
 */
@TableName(value ="evaluate")
@Data
public class Evaluate implements Serializable {
    /**
     * 评价id
     */
    @TableId(type = IdType.AUTO)
    private Long id;

    /**
     * 课id
     */
    private Long subjectId;

    /**
     * 评分（1-10分）
     */
    private Integer score;

    /**
     * 评价内容
     */
    private String content;

    /**
     * 评价时间
     */
    private Date evaluateTime;

    /**
     * 创建人
     */
    private Long createBy;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 修改人
     */
    private Long modifyBy;

    /**
     * 修改时间
     */
    private Date modifyTime;


}