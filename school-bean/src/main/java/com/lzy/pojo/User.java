package com.lzy.pojo;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @TableName user
 */
@TableName(value ="`user`")
@Data
public class User implements Serializable {
    /**
     * 用户ID
     */
    @TableId(value = "ID", type = IdType.AUTO)
    private Long id;


    /**
     * 用户名
     */
    private String name;

    /**
     * 班级id
     */
    private Long classId;

    /**
     * 年级id
     */

    private Long grateId;

    /**
     * 院系id
     */

    private Long facultiesId;

    /**
     * 密码
     */

    private String password;

    /**
     * 性别 0：男 1：女
     */

    private Integer sex;

    /**
     * 手机号
     */

    private String phone;

    /**
     * 身份证号
     */

    private String cardId;

    /**
     * 地址
     */

    private String address;

    /**
     * 生日
     */

    @DateTimeFormat(pattern = "")
    private Date birthday;

    /**
     * 邮箱
     */

    private String email;

    /**
     * 籍贯
     */

    private String origin;

    /**
     * 婚姻状况（0已婚  1未婚）
     */

    private Integer marriageStatus;

    /**
     * 政治面貌 0：群众 1：团员 2：党员
     */

    private Integer outlook;

    /**
     * 开始时间
     */

    private Date startDate;

    /**
     * 结束时间
     */

    private Date endDate;

    /**
     * 创建人
     */

    private String createBy;

    /**
     * 创建时间
     */

    private Date createTime;

    /**
     * 修改人
     */

    private String modifyBy;

    /**
     * 修改时间
     */

    private Date modifyTime;
    /**
     * 逻辑删除
     */
    @TableLogic(value = "0",delval = "1")
    private boolean deleteId;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}
