package com.lzy.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.Date;

import lombok.*;

/**
 * 签到表
 * @TableName attendance
 */
@TableName(value ="attendance")
@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class Attendance implements Serializable {
    /**
     * 学生id
     */
    private Long userId;

    /**
     * 学科id
     */
    private Long subjectId;

    /**
     * 教室id
     */
    private Long classroomId;

    /**
     * 状态（0已到  1未到  2请假）
     */
    private Integer status;

    /**
     * 创建人
     */
    private Long createBy;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 修改人
     */
    private Long modifyBy;

    /**
     * 修改时间
     */
    private Date modifyTime;

    private String name;


}