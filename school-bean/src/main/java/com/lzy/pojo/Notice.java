package com.lzy.pojo;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 通知公告表
 * @TableName notice
 */
@TableName(value ="notice")
@Data
public class Notice implements Serializable {
    /**
     * 公告ID
     */
    @TableId(type = IdType.AUTO)
    private Integer id;

    /**
     * 公告标题
     */
    private String title;

    /**
     * 公告类型（1通知 2公告）
     */
    private String typea;

    /**
     * 公告状态（0正常 1关闭）
     */
    private String status;

    /**
     * 创建者
     */
    private Long createBy;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 更新者
     */
    private Long modifyBy;

    /**
     * 更新时间
     */
    private Date modifyTime;

    /**
     * 备注
     */
    private String remark;

    /**
     * 公告内容
     */
    private String content;
    /**
     * 逻辑删除
     */
    @TableLogic
    private int deleteId;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}