package com.lzy.pojo;


import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 学生选课表
 * @TableName user_subject
 */
@TableName(value ="user_subject")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserSubject implements Serializable {
    /**
     * 用户id
     */
    @TableField(value = "USER_ID")
    private Long userId;

    /**
     * 学科id
     */
    @TableField(value = "SUBJECT_ID")
    private Long subjectId;
    @TableField(value = "TEA_SUBJECT_ID")
    private Long teaSubjectId;

    //    是否评价
    private Integer isEvaluate;
    private Integer isScore;

    public UserSubject (Long userId, Long subjectId ,Long teaSubjectId){
        this.userId = userId;
        this.subjectId = subjectId;
        this.teaSubjectId = teaSubjectId;
    }


}
