package com.lzy.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 课程表
 * @TableName lesson
 */
@TableName(value ="lesson")
@Data
public class Lesson implements Serializable {
    /**
     * 课程表id
     */
    @TableId(type = IdType.AUTO)
    private Long id;

    /**
     * 课id
     */
    private Long subjectId;

    /**
     * 课程（标题）
     */
    private String title;

    /**
     * 用户id(教师)
     */
    private Long userId;

    /**
     * 上课时间
     */
    private Date startTime;

    /**
     * 下课时间
     */
    private Date endTime;

    /**
     * 教室id
     */
    private Long calssroomId;

    /**
     * 课程开始时间
     */
    private Date srartDate;

    /**
     * 课程结束时间
     */
    private Date endDate;

    /**
     * 创建人
     */
    private Long create;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 修改人
     */
    private Long modify;

    /**
     * 修改时间
     */
    private Date modifyTime;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;


}