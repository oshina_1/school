package com.lzy.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 听课表
 * @TableName lectures
 */
@TableName(value ="lectures")
@Data
public class Lectures implements Serializable {
    /**
     * id
     */
    @TableId(type = IdType.AUTO)
    private Long id;
    /**
     * 教室id
     */
    private Long classroomId;

    /**
     * 教师id
     */
    private Long userId;

    /**
     * 课程id
     */
    private Long subjectId;

    /**
     * 评价人
     */
    private Long evaluateId;
    //    是否评价
    private Integer isEvaluate;



    /**
     * 听课时间
     */
    private Date lectureesTime;

    /**
     * 创建人
     */
    private Long create;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 修改人
     */
    private Long modify;

    /**
     * 修改时间
     */
    private Date modifyTime;


}