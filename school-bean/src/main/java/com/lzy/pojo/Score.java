package com.lzy.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 分数表
 * @TableName score
 */
@TableName(value ="score")
@Data
public class Score implements Serializable {
    /**
     * 成绩id
     */
    @TableId(type = IdType.AUTO)
    private Long id;

    /**
     * 用户id（学生）
     */
    private Long userId;

    /**
     * 课程id
     */
    private Long subjectId;

    /**
     * 平时成绩（40%）
     */
    private Integer scoreDaily;

    /**
     * 期末成绩（60%）
     */
    private Integer score;

    /**
     * 学期
     */
    private Long grateId;
    /**
     * 创建人
     */
    private Long createBy;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 修改人
     */
    private Long modifyBy;

    /**
     * 修改时间
     */
    private Date modifyTime;


}