package com.lzy.vo;

import com.baomidou.mybatisplus.annotation.TableLogic;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

/**
 * 用户信息增强类
 */
@Getter
@Setter
@ToString
public class UserVo {

    private Long id;

    private String username;

    private Long facultiesId;
    @TableLogic
    private Integer deleteId;

    private List<String> permissions;

    private List<String> roles;
}
