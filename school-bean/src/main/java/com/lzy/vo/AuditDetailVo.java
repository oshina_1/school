package com.lzy.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AuditDetailVo {
    private long id;
    private long diagramId;
    private long applyId;
    private long auditorId;
    private LocalDateTime auditTime;
    private LocalDateTime createTime;
    private long auditStatus;
    private String remark;
    private long applyType;
    private String title;

    private String applyName;
    private String auditName;
    private long userId;
}
