package com.lzy.vo;

import lombok.*;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class StuScoreVo {
    private Long userId;
    private String userName;
    private String className;
    private Long subjectId;
    private String subjectName;
    private Integer scoreDaily;
    private Integer score;
    private Long facultiesId;
}
