package com.lzy.vo;

import com.lzy.pojo.User;
import com.lzy.util.PageUtil;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class UserPageVo {

    private UserTVo user;

    private PageUtil page;

}
