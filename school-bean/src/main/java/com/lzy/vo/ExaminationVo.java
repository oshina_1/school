package com.lzy.vo;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.sql.Time;
import java.time.LocalDate;
import java.time.LocalTime;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ExaminationVo {

    private Long id;
    private Long gId;
    private String gName;
    private int semester;
    private Long sId;
    private String sName;
    private Long pId;
    private String pName;
    private LocalDate testDay;
    private LocalTime testStartTime;
    private LocalTime testEndTime;
    private Long cId;
    private String cName;
    private int testNumber;
    private Long leadId;
    private Long associateId;
    private String leadName;
    private String associateName;
    private int testMode;
    private String remark;
    private Long facultiesId;

}
