package com.lzy.vo;

import com.lzy.pojo.Classroom;
import com.lzy.util.PageUtil;
import lombok.*;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ClassroomPageVo {

    private ClassroomVo classroom;

    private PageUtil page;

}
