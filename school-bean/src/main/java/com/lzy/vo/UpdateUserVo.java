package com.lzy.vo;

import com.lzy.pojo.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UpdateUserVo {
      private long id;
      private String phone;
      private String address;
      private String email;
//      private String password;
      private Date birthday;
}
