package com.lzy.vo;

import com.lzy.util.PageUtil;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class PaperPageVo {

    private PaperVo paperVo;

    private PageUtil page;

}
