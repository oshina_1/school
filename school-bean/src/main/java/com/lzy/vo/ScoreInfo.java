package com.lzy.vo;

import lombok.*;

import java.util.Objects;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ScoreInfo {
    private Long grateId;
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ScoreInfo scoreInfo = (ScoreInfo) o;
        return Objects.equals(grateId, scoreInfo.grateId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(grateId);
    }
}
