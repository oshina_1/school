package com.lzy.vo;

import lombok.*;

import java.util.Date;
import java.util.List;
import java.util.Map;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor

@ToString
public class StatsVo {
    //开始时间
    private Date startTime;
    //科目id
    private Long subjectId;
    //状态
    private Integer status;
    //院系
    Long facultiesId;

}
