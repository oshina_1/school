package com.lzy.vo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.Date;

import lombok.*;

/**
 * 教师评价
 * @TableName evaluate
 */

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class EvaluateVo implements Serializable {
    /**
     * 课程名
     */
    private String subjectName;

    private String teacherName;
    /**
     * 评分（1-10分）
     */
    private Integer score;

    /**
     * 评价内容
     */
    private String content;

    /**
     * 评价时间
     */
    private Date evaluateTime;

    private String createName;




}