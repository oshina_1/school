package com.lzy.vo;

import lombok.*;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ScoreInfoVo {
    private Long subjectId;
    private String subjectName;
    private String studentName;
    private Long userId;
    private int credits;
    private int classHour;
    private Long grateId;

}
