package com.lzy.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class LoginVo {

    @NotEmpty(message = "学号不能为空")
    private  String userId;

    @NotEmpty(message = "密码号不能为空")
    private String password;

    @NotEmpty(message = "验证码不能为空")
    private String code;
    @NotEmpty(message = "uuid不能为空")
    private String uuid;
}
