package com.lzy.vo;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class UserTVo {

    private Long id;
    private Long facultiesId;
    private String name;
    private String fname;
    private Integer sex;
    private String phone;
    private String address;
    private LocalDate birthday;
    private String email;
    private String origin;
    private Integer marriageStatus;
    private Integer outlook;
    private LocalDate startDate;
    private LocalDate endDate;

}
