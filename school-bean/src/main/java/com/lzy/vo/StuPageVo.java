package com.lzy.vo;

import com.lzy.util.PageUtil;
import lombok.*;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class StuPageVo {

    private StudentVo studentVo;

    private PageUtil page;

}
