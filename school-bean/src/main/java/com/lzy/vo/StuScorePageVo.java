package com.lzy.vo;

import com.lzy.util.PageUtil;
import lombok.*;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class StuScorePageVo {

    private StuScoreVo stuScoreVo;

    private PageUtil page;

}
