package com.lzy.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SubjectVo {
    private Long subjectId;
    private String name;//学科名
    private int credits;
    private int classHour;
    private int type;

}
