package com.lzy.vo;

import lombok.*;

import java.time.LocalDate;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class StudentVo {
    private Long id;

    private Long subjectId;

    private Long grateId;

    private String name;

    private String className;

    private String grateName;

    private String facultiesName;

    private Integer sex;

    private String phone;

    private String cardId;

    private String address;

    private LocalDate birthday;

    private String email;

    private String origin;

    private Integer marriageStatus;

    private Integer outlook;

    private LocalDate startDate;

    private LocalDate endDate;
}
