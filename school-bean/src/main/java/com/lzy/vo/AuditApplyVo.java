package com.lzy.vo;

import com.lzy.pojo.AuditDetail;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AuditApplyVo {
    private long id;
    private String title;
    private long applyType;
    List<AuditNode> nodes;
    List<AuditDetail> auditNodes;
    private LocalDateTime applyTime;
    private long applyStatus;
    private String applyName;
    private String auditName;
    private String remark;
    private LocalDateTime auditTime;
    private long userId;
    private long applyId;
    private long auditorId;
}
