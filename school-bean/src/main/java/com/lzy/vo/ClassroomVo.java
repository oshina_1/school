package com.lzy.vo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.lzy.pojo.Classroom;
import com.lzy.util.PageUtil;
import lombok.*;

import java.util.Date;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ClassroomVo {

    private Long id;

    private String name;

    private String type;

    private Integer status;

    private Long facultiesId;

    private String facultiesName;

    private String position;

}
