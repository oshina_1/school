package com.lzy.vo;

import lombok.*;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class AttendanceVo {
    //学生签到状态
    private Integer status;
    //学生id
    private Long studentId;
    //学科id
    private Long subjectId;
    //学生名
    private String name;
    //缺勤总次数
    private Long count;
}
