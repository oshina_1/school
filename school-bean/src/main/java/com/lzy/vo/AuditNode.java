package com.lzy.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AuditNode {
    private Long  id;
    private Long auditorId;
    private String nodeName;
    private int nodeType;
    List<AuditNode> childrenNodes;
}
