package com.lzy.vo;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class PaperVo {

    private Long id;
    private String name;
    private String sName;
    private Long subjectId;
    private Long facultiesId;

}
