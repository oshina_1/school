package com.lzy.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SelectSubjectVo {

    private String subjectName;
    private Integer gradeName;
    private Integer selectSubjectNum; //选课人数
    private Integer status;//选课状态 -1未开始  0开始 1结束
    private String subjectTeacher;


}
