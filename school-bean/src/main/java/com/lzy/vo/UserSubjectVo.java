package com.lzy.vo;


import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 学生选课表
 * @TableName user_subject
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserSubjectVo implements Serializable {
    /**
     * 用户id
     */
    private Long userId;

    /**
     * 学科id
     */
    private Long subjectId;

    private Long teaSubjectId;

    //    是否评价
    private Integer isEvaluate;

    private String name;



}
