package com.lzy.vo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.*;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.Date;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ScoreVo implements Serializable {
    /**
     * 学期
     */
    private Long grateId;

    private String subjectName;
    private String teacher;
    //学分
    private Double credits;
    /**
     * 平时成绩（40%）
     */
    private Integer scoreDaily;

    /**
     * 期末成绩（60%）
     */
    private Integer score;


    private double sumScore;



    public void setSumScore(Integer scoreDaily,Integer score) {

        this.sumScore = scoreDaily * 0.4 + score * 0.6;
        // 创建DecimalFormat对象，指定要保留的小数位数
        DecimalFormat decimalFormat = new DecimalFormat("#.#");

        // 使用格式化器将sumScore保留一位小数
        String formattedSumScore = decimalFormat.format(this.sumScore);

        // 将格式化后的值解析为double类型
        this.sumScore = Double.parseDouble(formattedSumScore);
    }
}