package com.lzy.vo;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class PasswdVo {

    private Long id;
    private String newpassword;
    private String oldPassword;

}
