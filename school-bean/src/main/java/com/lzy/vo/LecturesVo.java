package com.lzy.vo;
import lombok.*;

import java.util.Date;
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class LecturesVo {
    private Long lecturesId;
    private String classroom;
    private String teacher;
    private String subjectName;
    private Date lecturesTime;
    private String evaluateName;
    private Long subjectId;
}