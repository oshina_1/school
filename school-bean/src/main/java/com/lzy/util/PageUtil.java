package com.lzy.util;

import lombok.*;

import java.util.List;


@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class PageUtil<T> {
    // 当前页
    private int pageNo;
    // 每页数据量
    private int pageSize;
    // 总页数
    private int totalPage;
    // 总数据量
    private int count;
    // 下标
    private int index;
    // 当前页的数据
    List<T> data;

    public void setCount(int count) {
        this.count = count;
        // 计算总页数
        int num = count / pageSize;
        if (count % pageSize != 0) {
            num++;
        }
        this.totalPage = num;
    }
}