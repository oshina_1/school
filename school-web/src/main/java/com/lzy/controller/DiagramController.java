package com.lzy.controller;

import com.lzy.pojo.AuditDiagram;
import com.lzy.pojo.User;
import com.lzy.result.Result;
import com.lzy.service.AuditDiagramService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/diagram")
@Slf4j
public class DiagramController {

    @Autowired
    private AuditDiagramService auditDiagramService;

    @PostMapping("/add")
    public String add(@RequestBody AuditDiagram diagram){
        log.info(diagram.toString());
        boolean save = auditDiagramService.save(diagram);
        return save ? "success" : "error";
    }

    @GetMapping("/selectTea")
    public Result<?> selectTea() {
        List<User> teas = auditDiagramService.selectTea();
        return Result.success(teas);
    }
}
