package com.lzy.controller;


import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.google.code.kaptcha.Producer;
import com.lzy.exception.BaseException;
import com.lzy.exception.PasswordErrorException;
import com.lzy.pojo.Faculties;
import com.lzy.pojo.Menu;
import com.lzy.pojo.Role;
import com.lzy.pojo.User;
import com.lzy.result.Result;
import com.lzy.service.FacultiesService;
import com.lzy.service.MenuService;
import com.lzy.service.RoleService;
import com.lzy.service.UserService;
import com.lzy.util.Base64;
import com.lzy.util.JwtUtil;
import com.lzy.util.Md5Utils;
import com.lzy.vo.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.util.FastByteArrayOutputStream;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import static com.lzy.constant.MessageConstant.ACCOUNT_NOT_FOUND;
import static com.lzy.constant.MessageConstant.PASSWORD_ERROR;
import static java.util.stream.Collectors.toList;

@RestController
@Slf4j
public class UserController {
    @Resource(name = "captchaProducerMath")
    private Producer captchaProducerMath;

    @Autowired
    private RedisTemplate redisTemplate;


    @Autowired
    private UserService userService;

    @Autowired
    private MenuService menuService;

    @Autowired
    private RoleService roleService;

    @Autowired
    private HttpServletRequest request;

    @Autowired
    private FacultiesService facultiesService;


    @PostMapping("/login")
    public Result<String> login(@RequestBody LoginVo loginVo) {
        log.info("传入对象为{}", loginVo);
        //查询用户
        String userId = loginVo.getUserId();
        String password = loginVo.getPassword();
        String code = loginVo.getCode();
        String uuid = loginVo.getUuid();

        Long id = Long.parseLong(userId);
        User user = userService.getById(id);
//        无用户,自动创建
        if (user == null) {
            throw new BaseException(ACCOUNT_NOT_FOUND);
        }

        //校验密码
        String password1 = user.getPassword();
        String hashPassword = Md5Utils.hash(password);
        if (!hashPassword.equals(password1)) {
            throw new PasswordErrorException(PASSWORD_ERROR);
        }

        //校验验证码
        String redisCode = redisTemplate.opsForValue().get("captcha_codes:" + uuid).toString();
        if (!StringUtils.hasLength(redisCode)) {
            throw new BaseException("验证码已过期");
        }
        if (!redisCode.equals(code)) {
            throw new BaseException("验证码错误!");

        }
        Long id1 = user.getId();
        //生成token并返回
        String tokenByUserId = JwtUtil.createTokenByUserId(id1);

        //将token存入redis

        redisTemplate.opsForValue().set("login:token:" + id1, tokenByUserId);

        return Result.success(tokenByUserId);

    }

    @GetMapping("/routes")
    public Result<?> routes() {
        String token = request.getHeader("token");
        // 使用JWT工具类获取用户对象
        Long userId = JwtUtil.getUserIdByToken(token);
        // User currentUser = new User();
        // currentUser.setId(1L);
        // 查出用户的角色权限信息，封装成路由对象
        List<Menu> menuTreeByUserId = menuService.findMenuTreeByUserId(userId);
        List<RouterVo> routerVos = menuService.buildRouters(menuTreeByUserId);
        return Result.success(routerVos);
    }

    @GetMapping("/getInfo")
    public Result<?> getInfo() {
        String token = request.getHeader("token");
        Long id = JwtUtil.getUserIdByToken(token);
        UserVo user = new UserVo();
        user.setId(id);
        user.setUsername(userService.findName(id));
        user.setFacultiesId(userService.findFacultiesId(id));

        // 获取权限
        List<String> perms = menuService.findAllByUserId(id)
                .stream()
                .map(Menu::getPerms)
                .collect(toList());
        user.setPermissions(perms);

        // 获取角色
        List<String> roles = roleService.findAllByUserId(id)
                .stream()
                .map(Role::getName)
                .collect(toList());
        user.setRoles(roles);
        return Result.success(user);
    }


    @PostMapping("updatePassword")
    public Result<?> updatePassword(@RequestBody PasswdVo passwdVo) {
        User user = userService.findUser(passwdVo.getId());
        if (user == null) {
            return Result.error("用户不存在");
        }
        String hashPassword = Md5Utils.hash(passwdVo.getOldPassword());
        if (!user.getPassword().equals(hashPassword)) {
            return Result.error("旧密码不匹配");
        }
        UpdateWrapper<User> updateWrapper = new UpdateWrapper<>();
        String hashPassword1 = Md5Utils.hash(passwdVo.getNewpassword());
        updateWrapper.eq("ID", passwdVo.getId())
                .set("PASSWORD", hashPassword1);
        boolean update = userService.update(updateWrapper);
        return Result.success(update);
    }


    @GetMapping("selectAll")
    @RequiresRoles("user")
    public Result<Page<User>> select(User user1, Integer current, Integer pageSize) {
//        List<User> list = userService.list();
//        return Result.success(list);
        Page<User> userPage = userService.getUserPage(user1, current, pageSize);

        return Result.success(userPage);
    }

    @PutMapping("updateUseer")
    public Result<?> update(UpdateUserVo updateUserVo) {
        boolean b = userService.updateUser(updateUserVo);
        return Result.success(b);
    }

    @PostMapping("insertUser")
    public Result<?> insert(@RequestBody User user,Long roleId) {
        if (user == null) {
            return Result.error("用户为空");
        }
        userService.insert(user,roleId);
        return Result.success("新增用户成功");
    }

    @PostMapping("deleteUser")
    public Result<?> delete(Long id) {
         userService.deleteId(id);
        return Result.success("删除成功");
    }


    /*
     * 获取系部信息
     *@param null
     *@return  || created at 2023/9/12 14:28
     */
    @GetMapping("/getFaculties")
    public Result getFaculties() {
        List<Faculties> list = facultiesService.list();
        return Result.success(list);

    }


    /*
     * 添加系部
     */
    @PostMapping("/addFaculty")
    public Result addFaculty(@RequestBody Faculties faculties) {
        log.info("添加系部...{}", faculties);
        Faculties name = facultiesService.query().eq("NAME", faculties.getName()).one();
        if (name!=null){
            return Result.error("系部已存在,无法添加!");

        }
        faculties.setCreateTime(LocalDateTime.now());
        facultiesService.save(faculties);
        return Result.success("添加系部成功!");

    }

    /*
    * 删除系部
    */
    @DeleteMapping("/deleteFaculty/{id}")
    public Result deleteFaculty(@PathVariable Long id){
        log.info("删除系部....{}",id);
        facultiesService.removeById(id);
        return Result.success("删除成功");



    }

    /*
     *
     * 生成验证码
     */
    @GetMapping("/captchaImage")
    public Result getCode() throws IOException {

        // 保存验证码信息
        String uuid = UUID.randomUUID().toString();
        String verifyKey = "captcha_codes:" + uuid;

        String capStr = null, code = null;
        BufferedImage image = null;

        // 生成验证码,2分钟

        String capText = null;

        capText = captchaProducerMath.createText();

        System.out.println(capText);
        capStr = capText.substring(0, capText.lastIndexOf("@"));
        code = capText.substring(capText.lastIndexOf("@") + 1);
        image = captchaProducerMath.createImage(capStr);
        System.out.println(image);


        redisTemplate.opsForValue().set(verifyKey, code, 2, TimeUnit.MINUTES);
        // 转换流信息写出
        FastByteArrayOutputStream os = new FastByteArrayOutputStream();
        try {
            ImageIO.write(image, "jpg", os);
        } catch (IOException e) {
            return Result.error(e.getMessage());
        }
        Map map = new HashMap();

        map.put("uuid", uuid);
        map.put("img", Base64.encode(os.toByteArray()));
        return Result.success(map);
    }


    @RequiresRoles("user")
    @GetMapping("/logout")
    public Result logout(HttpServletRequest servletRequest) {

        log.info("退出请求..........");
        String token = servletRequest.getHeader("token");
        Long userIdByToken = JwtUtil.getUserIdByToken(token);
        redisTemplate.delete("login:token:" + userIdByToken);

        return Result.success("退出成功!");
    }
}
