package com.lzy.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.lzy.pojo.AuditApply;
import com.lzy.pojo.AuditDetail;
import com.lzy.result.PageResult;
import com.lzy.result.Result;
import com.lzy.service.AuditApplyService;
import com.lzy.service.AuditDetailService;
import com.lzy.vo.AuditApplyVo;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/apply")
public class AuditApplyController{
    @Autowired
    private AuditApplyService auditApplyService;
    @Autowired
    private AuditDetailService auditDetailService;

    @RequestMapping("/applyFor")
    public String applyFor(@RequestBody AuditApply auditApply) {
        try {
            auditApplyService.apply(auditApply);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
        return  "success";
    }


    /*
     * 审核列表
     * */
    @RequestMapping("/auditList/{userId}")
    public Result<?> auditList(@PathVariable long userId){
        //根据审核人查询审核列表
        List<AuditApply> auditList = auditApplyService.findAuditList(userId);
        return Result.success(auditList);
    }

    @SneakyThrows
    @RequestMapping("/audit")
    public void audit(@RequestBody AuditDetail detail) {
        //审核
        auditApplyService.audit(detail);
//        auditDetailService.audit(detail);
    }

    //查看申请记录
    @RequestMapping("/degree")
    public Result<?> degree(@RequestParam(value = "userId") Long userId, @RequestParam(value = "page") Integer page) {
        IPage<AuditApplyVo> applyPage = new Page<>(page,5);//每页显示5条数据

        List<AuditApplyVo> auditApplyVoList = auditApplyService.degree(applyPage,userId);
        long total = applyPage.getTotal();
        PageResult pageResult = new PageResult(total, auditApplyVoList);
        return  Result.success(pageResult);
    }

    //查看进度待审核的状态
    @RequestMapping("/degreeTwo/{userId}")
    public Result<?> degreeTwo(@PathVariable Long userId) {
        List<AuditApplyVo> auditApplyVoList = auditApplyService.degreeTwo(userId);
        return  Result.success(auditApplyVoList);
    }


    @RequestMapping("/applyCancel/{id}")
    public void applyCancel(@PathVariable long id)  {
        //撤销
        boolean isCancel = auditApplyService.cancel(id);
    }

//    @RequestMapping("/notice/{userId}")
//    public Result<?> notice(@PathVariable long userId){
//        int a  = auditApplyService.applyNotice(userId);
//        return Result.success(a);
//    }
}