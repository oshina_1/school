package com.lzy.controller;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lzy.mapper.SubjectMapper;
import com.lzy.mapper.UserMapper;
import com.lzy.pojo.Subject;
import com.lzy.pojo.User;
import com.lzy.pojo.UserSubject;
import com.lzy.result.PageResult;
import com.lzy.result.Result;
import com.lzy.service.StudentService;
import com.lzy.service.SubjectService;
import com.lzy.service.UserSubjectService;
import com.lzy.util.JwtUtil;
import com.lzy.vo.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
//import org.apache.shiro.authz.annotation.RequiresPermissions;

@RestController
@RequestMapping("/student")
@Slf4j
public class StudentController {
    @Autowired
    private StudentService userService;

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private SubjectMapper subjectMapper;

    @Autowired
    private UserSubjectService userSubjectService;


    @Autowired
    private SubjectService subjectService;

    //查询个人信息
    @GetMapping("/selectInfo/{stuId}")
    public Result<?> selectInfo(@PathVariable Long stuId){
        StudentVo studentVo = userService.selectInfo(stuId);
        return Result.success(studentVo);
    }

//    @RequiresPermissions("user:stu")
    @PostMapping("/update")
    //学生信息维护
    public Result<?> updateStudentInfo(@RequestBody StudentVo studentVo){
        if(userService.updateStudentInfo(studentVo)){
            return Result.success();
        }
        return Result.error("失败");
    }

    @GetMapping("/stuSubject/{userId}")
    public Result<?> stuSubject(@PathVariable Long userId){
        List<UserSubjectVo> subjects = userService.allStuSubject(userId);
        return Result.success(subjects);
    }

    @GetMapping("/selectSubInfo")
    public Result<?> selectSubInfo(@RequestParam(value ="facultiesId" ) Long facultiesId,@RequestParam Long userId){
        List<SubjectVo> subjects = userService.selectSubjectByUserId(facultiesId,userId);
        return Result.success(subjects);
    }
    @PostMapping("/updateSubjectId")
    public Result<?> updateSubjectId(@RequestBody UserSubject userSubject){
       String msg = userService.updateBySubjectId(userSubject.getSubjectId());
        return Result.success(msg);
    }

    @GetMapping("/selectScore")
    public Result<?> selectTeaDir(@RequestParam Long userId, @RequestParam int current,
                                  @RequestParam Long grateId) {
        IPage<ScoreVo> page = new Page<>(current,8);
        List<ScoreVo> scoreVos = userService.selectScoreByUserIdAndSemester(page,userId,grateId);
        long total = page.getTotal();
        PageResult pageResult = new PageResult(total,scoreVos);

        return Result.success(pageResult);
    }

    @GetMapping("/selectGrate/{userId}")
    public Result<?> selectGrate(@PathVariable Long  userId){
        List<ScoreInfo> grateId = userService.selectGrateByUserIdr(userId);
        return Result.success(grateId);
    }



    @GetMapping("/showSelectSubjects/{id}")
    public Result showSelectSubjects(@PathVariable Long id){
        log.info("学生查询展示所有的选修课的信息...{}",id);
        List<SubjectVo> subjectVos = userMapper.showSelectSubjects(id);
        return Result.success(subjectVos);
    }


    /*
    * 获取选课信息
    */
    @GetMapping("/getSubjectInfo/{id}")
    public Result getSubjectInfo(@PathVariable Long id){
        //获取当前年级下所有可用选修的课程
        User user = userService.getById(id);
        LambdaQueryWrapper<Subject> subjectLambdaQueryWrapper = new LambdaQueryWrapper<>();
        subjectLambdaQueryWrapper.eq(Subject::getGrateId,user.getGrateId())
                .eq(Subject::getType,-1)
                .in(Subject::getStatus,-1,0);
        List<Subject> list = subjectService.list(subjectLambdaQueryWrapper);
        return Result.success(list);
    }



    @PutMapping("/confirmSelectSubject/{id}")
    public  Result confirmSelectSubject(@PathVariable Long id,@RequestHeader("token") String token){
        log.info("学生确认选课中.......{}",id);
        //判断课程信息
        Subject subject = subjectService.getById(id);
        if (subject.getStatus()==-1){
            return Result.error("课程未开始,无法选择");
        }
        if (subject.getStudentNum()!=null){
            //统计当前课程所选的人数(年级,学生,课程id,name)
          Integer count = subjectMapper.confirmSelectSubject( subject);
          if (count>subject.getStudentNum()){
              return Result.error("课程人数已满");
          }

        }
        UserSubject userSubject = new UserSubject();
        userSubject.setSubjectId(subject.getId());
        userSubject.setUserId(JwtUtil.getUserIdByToken(token));
        userSubjectService.save(userSubject);

        return  Result.success("选课成功!");

    }



}
