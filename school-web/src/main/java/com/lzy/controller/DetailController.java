package com.lzy.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lzy.result.PageResult;
import com.lzy.result.Result;
import com.lzy.service.AuditDetailService;
import com.lzy.vo.AuditDetailVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/detail")
public class DetailController {

    @Autowired
    private AuditDetailService auditDetailService;

    //查询所有的审批记录
    @RequestMapping("/auditDetails")
    public Result<?> auditDetailList(@RequestParam(value = "auditorId") Long auditorId, @RequestParam(value = "page") Integer page) {
        IPage<AuditDetailVo> detailPage = new Page<>(page,5);//每页显示5条数据
        List<AuditDetailVo> detailList = auditDetailService.findDetailsByApplyId( detailPage,auditorId);
        long total = detailPage.getTotal();
        PageResult pageResult = new PageResult(total,detailList);
        return  Result.success(pageResult);
    }

    //审核页面通知
    @RequestMapping("/notice/{userId}")
    public Result<?> notice(@PathVariable long userId) {
        int a =  auditDetailService.auditNotice(userId);
        return Result.success(a);
    }
}
