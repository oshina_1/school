package com.lzy.controller;

import com.lzy.result.Result;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.UUID;

@RestController
@Slf4j
@RequestMapping("/common")
public class FilesController {

    @Value("${upload.path}")
    private String basePath;


    /*
     *文件上传
     *@param
     *@return
     */
    @PostMapping("/upload")
    public Result<String> upload(MultipartFile file) {
        //file是个历史文件,需要转存到指定位置,否则本次请求完成后文件 会被删除
        log.info("文件上传.....{}", file.toString());
        //原始文件名
        String originName = file.getOriginalFilename();//abc.jpg
        String suffix = originName.substring(originName.lastIndexOf("."));//获取到后缀.jpg
        //使用uuid重新生成行分拣
        String newName = UUID.randomUUID().toString() + suffix;
        //创建一个目录对象
        File dir = new File(basePath);
        //判断当前目录是否存在
        if (!dir.exists()) {
            //如果不存在,则创建目录
            dir.mkdirs();
        }
        try {
            file.transferTo(new File(basePath + newName));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return Result.success(newName);
    }

    @GetMapping("/download")
    public void download(String name, HttpServletResponse response) {
        log.info("下载文件.......................");
        //通过输入流读取文件内容
        try {
            FileInputStream fileInputStream = new FileInputStream(new File(basePath + name));
            //通过输出流将文件写回到浏览器
            ServletOutputStream outputStream = response.getOutputStream();
            int len=0;
            byte[] bytes = new byte[1024];
            while((len=fileInputStream.read(bytes))!=-1){
                outputStream.write(bytes,0,len);
                outputStream.flush();
            }
            //释放资源
            outputStream.close();
            fileInputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}
