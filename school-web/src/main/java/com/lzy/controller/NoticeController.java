package com.lzy.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lzy.mapper.NoticeMapper;
import com.lzy.pojo.Notice;
import com.lzy.pojo.User;
import com.lzy.result.Result;
import com.lzy.service.NoticeService;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiresRoles("user")
public class NoticeController {
    @Autowired
    private NoticeService noticeService;
    @GetMapping("/selectAllNotice")
    public Result<Page<Notice>> selectNotice(Notice notice, Integer current, Integer pageSize){
        Page<Notice> notices = noticeService.selectAll(notice,current,pageSize);
        return Result.success(notices);
    }
    @GetMapping("/selectIdNotice")
    public Result<?> selectNotice(Long id){
        Notice byId = noticeService.findById(id);
        return Result.success(byId);
    }
    @PostMapping("/insertNotice")
    public Result<?> insertNotice(@RequestBody Notice notice){
        noticeService.insert(notice);
        return Result.success("新增成功");
    }
    @GetMapping("deleteNotice")
    public Result<?> delete( Long id){
        boolean b = noticeService.delete(id);
        return Result.success(b);
    }

    @GetMapping("/findNotice")
    public Result<?> findNotice() {
        List<Notice> notice = noticeService.findNotice();
        return Result.success(notice);
    }

    @GetMapping("/findNotify")
    public Result<?> findNotify() {
        List<Notice> notify = noticeService.findNotify();
        return Result.success(notify);
    }
}
