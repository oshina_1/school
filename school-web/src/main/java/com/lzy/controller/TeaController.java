package com.lzy.controller;

import com.lzy.result.Result;
import com.lzy.service.TeaService;
import com.lzy.service.UserService;
import com.lzy.util.PageUtil;
import com.lzy.vo.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping("/tea")
@RestController
@Slf4j
public class TeaController {

    @Autowired
    private TeaService teaService;
    @Autowired
    private UserService userService;

    @GetMapping("/selectTeaDir")
    public Result selectTeaDir(@RequestParam(value = "subjectName", required = false) String subjectName,
                               @RequestParam(value = "roleId")Long roleId,
                               @RequestParam(value = "teacherName", required = false) String teacherName,
                               @RequestParam Long facultiesId){
        List<EvaluateVo> evaluateVo = teaService.findTeaEvaBySubjectId
                (subjectName, roleId ,teacherName,facultiesId);
        if(evaluateVo != null){
            return Result.success(evaluateVo);
        }
        return Result.error("无此信息");

    }
    @GetMapping("/facultiesIdByUId/{userId}")
    public Result facultiesIdByUId(@PathVariable Long userId){
        return Result.success(userService.findfacultiesIdByUserId(userId));
    }

    @PostMapping("stuScore")
    public Result<?> stuScore(@RequestBody StuScorePageVo stuScorePageVo){
        teaService.getStuScore(stuScorePageVo);
        if (stuScorePageVo.getPage() != null) {
            PageUtil page = stuScorePageVo.getPage();
            return Result.success(page);
        }
        return Result.error("查询失败");
    }

    @PostMapping("findExamination")
    public Result<?> findExamination(@RequestBody ExaminationPageVo examinationPageVo){
        teaService.findExamination(examinationPageVo);
        if (examinationPageVo.getPage() != null) {
            PageUtil page = examinationPageVo.getPage();
            return Result.success(page);
        }
        return Result.error("未查询到数据");
    }

    @GetMapping("selectPaper/{facultiesId}")
    public Result<?> selectPaper(@PathVariable Long facultiesId){
        return Result.success(teaService.selectPaper(facultiesId));
    }

    @GetMapping("selectClassroom")
    public Result<?> selectClassroom(){
        return Result.success(teaService.selectClassroom());
    }

    @GetMapping("selectSubject/{facultiesId}")
    public Result<?> selectSubject(@PathVariable Long facultiesId){
        return Result.success(teaService.selectSubject(facultiesId));
    }

    @GetMapping("selectLead/{facultiesId}")
    public Result<?> selectLead(@PathVariable Long facultiesId){
        return Result.success(teaService.selectLead(facultiesId));
    }

    @GetMapping("selectAssociate/{facultiesId}")
    public Result<?> selectAssociate(@PathVariable Long facultiesId){
        return Result.success(teaService.selectAssociate(facultiesId));
    }

    @PostMapping("deleteInv/{id}")
    public Result<?> deleteInv(@PathVariable Long id){
        int i = teaService.deleteInv(id);
        if (i != 0) {
            return Result.success(i);
        }
        return Result.error("删除失败");
    }

    @PostMapping("updateInv")
    public Result<?> updateInv(@RequestBody ExaminationVo examinationVo){
        int i = teaService.updateInv(examinationVo);
        if (i != 0) {
            return Result.success(i);
        }
        return Result.error("修改失败");
    }

    @PostMapping("insertInv")
    public Result<?> insertInv(@RequestBody ExaminationVo examinationVo){
        int i = teaService.insertInv(examinationVo);
        if (i != 0) {
            return Result.success(i);
        }
        return Result.error("修改失败");
    }

    @PostMapping("findPaper")
    public Result<?> findPaper(@RequestBody PaperPageVo paperPageVo){
        teaService.findPaper(paperPageVo);
        if (paperPageVo.getPage() != null) {
            PageUtil page = paperPageVo.getPage();
            return Result.success(page);
        }
        return Result.error("未查询到数据");
    }

    @PostMapping("deletePaper/{id}")
    public Result<?> deletePaper(@PathVariable Long id){
        int i = teaService.deletePaper(id);
        if (i != 0) {
            return Result.success(i);
        }
        return Result.error("删除失败");
    }

    @PostMapping("insertPaper")
    public Result<?> insertPaper(@RequestBody PaperVo paperVo){
        int i = teaService.insertPaper(paperVo);
        if (i != 0) {
            return Result.success(i);
        }
        return Result.error("添加失败");
    }

    @PostMapping("updatePaper")
    public Result<?> updatePaper(@RequestBody PaperVo paperVo){
        int i = teaService.updatePaper(paperVo);
        if (i != 0) {
            return Result.success(i);
        }
        return Result.error("修改失败");
    }

    @PostMapping("findClassroom")
    public Result<?> findClassroom(@RequestBody ClassroomPageVo classroomPageVo){
        teaService.findClassroom(classroomPageVo);
        if (classroomPageVo.getPage() != null) {
            PageUtil page = classroomPageVo.getPage();
            return Result.success(page);
        }
        return Result.error("未查询到数据");
    }

    @PostMapping("findUser")
    public Result<?> selectUser(@RequestBody UserPageVo userPageVo){
        teaService.selectUser(userPageVo);
        if (userPageVo.getPage() != null) {
            PageUtil page = userPageVo.getPage();
            return Result.success(page);
        }
        return Result.error("未查询到数据");
    }

}
