package com.lzy.controller;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lzy.pojo.Lesson;
import com.lzy.pojo.Subject;
import com.lzy.pojo.User;
import com.lzy.pojo.UserSubject;
import com.lzy.result.Result;
import com.lzy.service.ClassroomService;
import com.lzy.service.SubjectService;
import com.lzy.service.UserService;
import com.lzy.service.UserSubjectService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@Slf4j
@RequestMapping("/lesson")
public class LessonController {

    @Autowired
    private SubjectService subjectService;


    @Autowired
    private ClassroomService classroomService;


    @Autowired
    private UserSubjectService userSubjectService;

    @Autowired
    private UserService userService;


    @GetMapping("/getSubject")
    public Result getSubject(Integer page, Integer size,
                             String name,
                             String grateId,
                             String type) {
        log.info("分页查询,{},{},{},{},{}", page, size, name, grateId, type);
        Page<Subject> subjectPage = new Page<>(page, size);
        LambdaQueryWrapper<Subject> subjectLambdaQueryWrapper = new LambdaQueryWrapper<>();
        subjectLambdaQueryWrapper.orderByDesc(Subject::getStudentNum);
        if (!"".equals(name)) {
            subjectLambdaQueryWrapper
                    .like(Subject::getName, name);
        }
        if (!"".equals(grateId)) {
            subjectLambdaQueryWrapper.eq(Subject::getGrateId, Long.parseLong(grateId));
        }
        if (!"".equals(type)) {
            subjectLambdaQueryWrapper.eq(Subject::getType, Integer.parseInt(type));
        }
        subjectService.page(subjectPage, subjectLambdaQueryWrapper);
        return Result.success(subjectPage);
    }


    @PutMapping("/addSubject/{id}")
    @Transactional
    public Result addSubject(@RequestBody Subject subject, @PathVariable Long id) {
        log.info("新增课程.......{},{}", subject, id);
        Subject one = subjectService.query().eq("NAME", subject.getName()).eq("GRATE_ID", subject.getGrateId()).one();
        if (one != null) {
            return Result.error("课程已存在,添加失败");
        }
        if (subject.getType() == -1) {
            subject.setStudentNum(200);
        }
        subject.setCreateTime(new Date(System.currentTimeMillis()));
        subject.setStatus(0);
        subjectService.save(subject);

        userSubjectService.save(new UserSubject(id, subject.getId(), subject.getId()));

        return Result.success("新增成功");
    }

    @DeleteMapping("/deleteSubject/{id}")
    @Transactional
    public Result deleteSubject(@PathVariable Long id) {
        log.info("删除课程....{}", id);
        Subject subject = subjectService.getById(id);
        List<UserSubject> subject_id = userSubjectService.query().eq("SUBJECT_ID", subject.getId()).list();
        List<Long> ids = subject_id.stream().map(UserSubject::getSubjectId).collect(Collectors.toList());
        userSubjectService.deleteByIds(ids);
        subjectService.removeById(id);
        return Result.success();
    }

    @GetMapping("/teachers")
    public Result getUsers() {
        List<User> userList = userService.findAllTeachers();
        return Result.success(userList);

    }

    @PostMapping("/createLesson")
    public Result createLesson(@RequestBody Lesson lesson) {
        log.info("创建课表...........{}", lesson);


        return null;
    }


}
