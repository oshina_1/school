package com.lzy.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lzy.pojo.Classroom;
import com.lzy.result.Result;
import com.lzy.service.ClassroomService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ClassroomController {
    @Autowired
    private ClassroomService classroomService;
    @GetMapping("selectClassroom")
    public Result<?> selectAll(String name,Integer currenter,Integer pageSizer){
        Page<Classroom> classPage = classroomService.getClassPage(name,currenter, pageSizer);
        return Result.success(classPage);
    }
    @PostMapping("insertClass")
    public Result<?> insert(@RequestBody Classroom classroom){
        boolean b = classroomService.insertClass(classroom);
        return Result.success(b);
    }
    @GetMapping("deleteClass")
    public Result<?> delete(Long id){
        boolean b = classroomService.removeById(id);
        return Result.success(b);
    }
}
