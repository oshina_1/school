package com.lzy.controller;

import com.lzy.pojo.*;
import com.lzy.result.PageResult;
import com.lzy.result.Result;
import com.lzy.service.LecturesService;
import com.lzy.service.ScoreService;
import com.lzy.service.TeacherService;
import com.lzy.service.UserService;
import com.lzy.util.PageUtil;
import com.lzy.vo.*;
import com.lzy.util.PageUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RequestMapping("/teacher")
@RestController
@Slf4j
public class TeacherController {

    @Autowired
    private TeacherService teacherService;
    @Autowired
    private LecturesService lecturesService;
    @Autowired
    private ScoreService scoreService;


    //获取自己所有的课程
    @GetMapping("/allTeaSubject/{teaId}")
    public Result<?> allTeaSubject (@PathVariable Long teaId){
        List<UserSubjectVo> teaSub = teacherService.allTeaSubject(teaId);
        return Result.success(teaSub);
    }
    //所有选择该课程的所有学生信息

    @GetMapping("/stuAttendance/{subjectId}")
    public Result<?> stuAttendance (@PathVariable Long subjectId){
        List<AttendanceVo> attendances = teacherService.findAllStudentBySubId(subjectId);
        return Result.success(attendances);
    }

    //修改签到学生状态

    @PostMapping("/updateAttStatus")
    public Result<?> updateAttStatus(@RequestBody AttendanceVo attendanceVo){
        int i = teacherService.updateStudentAttendanceByStuId(attendanceVo.getStatus(),
                attendanceVo.getStudentId(), attendanceVo.getSubjectId());
        if(i > 0){
            return Result.success();
        }
        return Result.error("修改失败");
    }

    //教师查询所有选课信息

    @PostMapping("/allSubject")
    public Result<?> allSubject (@RequestBody PageUtils pageUtils){
       List<Subject> subjects = teacherService.allSubject(pageUtils);
       Long total = teacherService.subCount(pageUtils.getQuery());
        PageResult page = new PageResult(total,subjects);
        return Result.success(page);
    }
    @GetMapping("/allSubject")
    public Result<?> allSubject (){
        List<Subject> subjects = teacherService.allSubject();
        return Result.success(subjects);
    }

    //教师添加课程到自己的课程表中

    @PostMapping("/addSubject")
    public Result<?> addSubject(@RequestBody UserSubject userSubject){
        String msg = teacherService.teaAddSubject(userSubject);
        return Result.success(msg);
    }

    //教师统计学生出勤率，根据课程和时间段

    @PostMapping("/statsStu")
    public Result<?> statsStu(@RequestBody StatsVo statsVo){
        List<AttendanceVo> attendanceVos = teacherService.queryStuAttendInfo(statsVo.getStatus(),
                statsVo.getFacultiesId(),statsVo.getStartTime(),statsVo.getSubjectId());

        return Result.success(attendanceVos);
    }

    @PutMapping("/addEvaluate")
    public Result<?> addEvaluate(@RequestBody Evaluate evaluate){
        String msg = teacherService.teaEvaTeaBySubId(evaluate);
        return Result.success(msg);
    }
    @GetMapping("/selectSubject")
    public Result<?> selectSubjectByName(@RequestParam(value = "name",required = false) String name){
        List<Subject> subjects = teacherService.listLikeSelectByName(name);
        return Result.success(subjects);
    }

    @PostMapping("/classroomQuery")
    public Result<?> classroomQuery(@RequestBody ClassroomPageVo classroomPageVo){
        teacherService.classroomQuery(classroomPageVo);
        if (classroomPageVo.getPage() != null) {
            PageUtil page = classroomPageVo.getPage();
            return Result.success(page);
        }
        return Result.error("查询失败");
    }

    @GetMapping("/selectFac")
    public Result<?> selectFac(){
        List<Faculties> faculties = teacherService.selectFac();
        return Result.success(faculties);
    }
    @GetMapping("/selectTeaDir")
    public Result<?> selectTeaDir(@RequestParam(value = "subjectId", required = false)Long subjectId,
                                  @RequestParam(value = "roleId",required = false)Long roleId,
                                  @RequestParam(value = "facultiesId",required = false) Long facultiesId,
                                  @RequestParam(value = "userId",required = false) Long userId,
                                  @RequestParam(value = "createBy",required = false) Long createBy) {
        List<EvaluateVo> evaluateVo = teacherService.findEvaBySubjectId
                (subjectId, roleId, facultiesId, userId, createBy);
        if (evaluateVo != null) {
            return Result.success(evaluateVo);
        }
        return Result.error("无此信息");
    }
    @GetMapping("/selectLectures/{userId}")
    public Result<?> selectLecturesByUserId(@PathVariable Long userId){
        List<LecturesVo> lectures = lecturesService.myLectures(userId);
        return Result.success(lectures);
    }
    @PostMapping("/updateLectures")
    public Result<?> updateLectures(@RequestBody LecturesVo lecturesVo){
        if(lecturesService.updateLectures(lecturesVo.getLecturesId())){
        return Result.success();
        }
        return Result.error("更新失败");
    }

    @PostMapping("/selectStuBySub")
    public Result<?> selectStuBySub(@RequestBody StuPageVo stuPageVo){
        teacherService.selectStuBySub(stuPageVo);
        if (stuPageVo.getPage() != null) {
            PageUtil page = stuPageVo.getPage();
            return Result.success(page);
        }
        return Result.error("查询失败");
    }

    @PostMapping("/selectStuScorePageByClassId")
    public Result<?> selectStuScorePageByClassId(@RequestBody StuScorePageVo stuScorePageVo){
        teacherService.selectStuScorePageByClassId(stuScorePageVo);
        if (stuScorePageVo.getPage() != null) {
            PageUtil page = stuScorePageVo.getPage();
            return Result.success(page);
        }
        return Result.error("查询失败");
    }

    @GetMapping("/selectStuScore")
    public Result<?> selectStuScore(@RequestParam Long subjectId, @RequestParam Long facultiesId){
        List<ScoreInfoVo> list = teacherService.selectScoreByFacAndUserId(subjectId,facultiesId);
        return Result.success(list);
    }

    @PostMapping("/addScore")
    public Result<?> addScore(@RequestBody List<Score> scores){
        for (Score score : scores) {
            scoreService.save(score);
        }
        return Result.success("提交成功");
    }
    @PutMapping("/updateIsScore")
    public Result<?> updateIsScore(@RequestBody List<UserSubject> userSubjects){
        for (UserSubject score : userSubjects) {
            teacherService.updateIsScore(score.getUserId(),score.getSubjectId());
        }
        return Result.success();
    }

}
