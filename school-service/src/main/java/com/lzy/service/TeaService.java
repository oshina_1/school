package com.lzy.service;

import com.lzy.pojo.User;
import com.lzy.vo.*;

import java.util.List;

public interface TeaService {
    List<EvaluateVo> findTeaEvaBySubjectId(String subjectName, Long roleId, String teacherName, Long facultiesId);
    void getStuScore(StuScorePageVo stuScorePageVo);
    void findExamination(ExaminationPageVo examinationPageVo);

    List<ExaminationVo> selectPaper(Long facultiesId);

    List<ExaminationVo> selectClassroom();

    List<ExaminationVo> selectSubject(Long facultiesId);

    int deleteInv(Long id);

    List<ExaminationVo> selectLead(Long facultiesId);

    List<ExaminationVo> selectAssociate(Long facultiesId);

    int updateInv(ExaminationVo examinationVo);

    int insertInv(ExaminationVo examinationVo);

    void findPaper(PaperPageVo paperPageVo);

    int deletePaper(Long id);

    int insertPaper(PaperVo paperVo);

    int updatePaper(PaperVo paperVo);

    void findClassroom(ClassroomPageVo classroomPageVo);

    void selectUser(UserPageVo userPageVo);
}
