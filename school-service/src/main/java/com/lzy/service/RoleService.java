package com.lzy.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lzy.pojo.Role;

import java.util.Collection;
import java.util.List;

public interface RoleService{
    List<Role> findAllByUserId(long id);
}
