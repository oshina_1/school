package com.lzy.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.lzy.pojo.Score;
import com.lzy.pojo.User;
import com.lzy.pojo.UserSubject;
import com.lzy.result.Result;
import com.lzy.vo.*;

import java.util.List;

/**
* @author zyj
* @description 针对表【user】的数据库操作Service
* @createDate 2023-08-29 15:54:15
*/
public interface StudentService extends IService<User> {

    StudentVo selectInfo(Long stuId);
    boolean updateStudentInfo(StudentVo studentVo);

    List<UserSubjectVo> allStuSubject(Long userId);

    //根据课程id找到对应课程的评价信息
    List<SubjectVo> selectSubjectByUserId(Long facultiesId,Long userId);

    String updateBySubjectId(Long subjectId);
//分页查询学生成绩
    List<ScoreVo> selectScoreByUserIdAndSemester(IPage<ScoreVo> page, Long userId, Long grateId);
    List<ScoreInfo> selectGrateByUserIdr(Long userId);

}
