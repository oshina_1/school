package com.lzy.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lzy.pojo.UserSubject;
import com.lzy.vo.UserSubjectVo;

import java.util.List;

/**
* @author Administrator
* @description 针对表【user_subject(学生选课表)】的数据库操作Service
* @createDate 2023-09-02 11:05:54
*/
public interface UserSubjectService extends IService<UserSubject> {

    List<UserSubjectVo> allTeaSubject (Long teaId);
    List<UserSubjectVo> allStuSubject (Long userId);
    String findSubject(UserSubject userSubject);

    boolean updateBySubjectId(Long subjectId);

    void deleteByIds(List<Long> ids);
    void updateIsScore(Long userId, Long subjectId);
}
