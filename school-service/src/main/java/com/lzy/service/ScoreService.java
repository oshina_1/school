package com.lzy.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.lzy.pojo.Score;
import com.baomidou.mybatisplus.extension.service.IService;
import com.lzy.vo.ScoreInfo;
import com.lzy.vo.ScoreInfoVo;
import com.lzy.vo.ScoreVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
* @author Administrator
* @description 针对表【score(分数表)】的数据库操作Service
* @createDate 2023-09-14 11:04:54
*/
public interface ScoreService extends IService<Score> {

    List<ScoreVo> selectScoreByUserIdAndSemester(IPage<ScoreVo> page, Long userId, Long grateId);
    List<ScoreInfo> selectGrateByUserIdr(Long userId);

    List<ScoreInfoVo> selectScoreByFacAndUserId(Long subjectId, Long facultiesId);

}
