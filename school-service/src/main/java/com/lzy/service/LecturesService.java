package com.lzy.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lzy.pojo.Lectures;
import com.lzy.vo.LecturesVo;

import java.util.List;

/**
* @author Administrator
* @description 针对表【lectures(听课表)】的数据库操作Service
* @createDate 2023-09-02 11:29:34
*/
public interface LecturesService extends IService<Lectures> {

    List<LecturesVo> myLectures(Long userId);
    boolean updateLectures(Long id);
}
