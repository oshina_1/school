package com.lzy.service;

import com.lzy.pojo.Menu;
import com.lzy.vo.RouterVo;

import java.util.Collection;
import java.util.List;

public interface MenuService {

    List<Menu> findMenuTreeByUserId(long id);

    List<RouterVo> buildRouters(List<Menu> permissions);

    List<Menu> findAllByUserId(long id);
}
