package com.lzy.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.lzy.pojo.Notice;
import com.lzy.pojo.User;

import java.util.List;


/**
* @author 24846
* @description 针对表【notice(通知公告表)】的数据库操作Service
* @createDate 2023-09-01 14:44:35
*/
public interface NoticeService extends IService<Notice> {
    Page<Notice> selectAll(Notice notice,Integer current, Integer pageSize);

    Notice findById(Long id);

    void insert(Notice notice);

    Page<Notice> getUserPage(int pageNUm, int pageSize);
    boolean delete(Long id);

    List<Notice> findNotice();

    List<Notice> findNotify();
}
