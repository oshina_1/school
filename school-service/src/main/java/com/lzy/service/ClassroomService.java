package com.lzy.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.lzy.pojo.Classroom;
import com.lzy.pojo.Notice;

import java.util.List;

/**
* @author 24846
* @description 针对表【classroom】的数据库操作Service
* @createDate 2023-09-01 14:44:35
*/
public interface ClassroomService extends IService<Classroom> {
    void insertRoom(Classroom classroom);
    List<Classroom> selectRoom(Classroom classroom);
    Page<Classroom> getClassPage(String name,Integer currenter, Integer pageSizer);
    boolean insertClass(Classroom classroom);
    boolean deleteClass(Long id);


}
