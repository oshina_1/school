package com.lzy.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.lzy.pojo.User;
import com.lzy.vo.UpdateUserVo;
import com.lzy.vo.UserVo;

import java.util.List;

/**
* @author zyj
* @description 针对表【user】的数据库操作Service
* @createDate 2023-08-29 15:54:15
*/
public interface UserService extends IService<User> {

    //获取系部id -》 根据当前用户id
    Long findfacultiesIdByUserId(Long userId);

    String findName(Long id);
    User findUser(Long id);
    void updatePassword(Long id,String newpassword);
//    List<User> selectUser(User user);

    Page<User> getUserPage(User user1, Integer current, Integer pageSize);

    int insert(User user,Long roleId);
    boolean deleteId(Long id);
    boolean updateUser(UpdateUserVo updateUserVo);


    Long findFacultiesId(Long id);

    List<User> findAllTeachers();
}
