package com.lzy.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lzy.pojo.AuditDiagram;
import com.lzy.pojo.User;
import org.springframework.stereotype.Service;

import java.util.List;


public interface AuditDiagramService extends IService<AuditDiagram> {
    List<User> selectTea();
}
