package com.lzy.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lzy.pojo.Attendance;
import com.lzy.vo.AttendanceVo;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

/**
* @author Administrator
* &#064;description  针对表【subject(学科表)】的数据库操作Service
* &#064;createDate  2023-09-01 13:55:06
 */
public interface AttendanceService extends IService<Attendance> {

    //根据教师id获取学生签到信息
    List<AttendanceVo> findAllStudentByTeaId(Long subjectId);

    //教学点名统计
//    String stuAttend();

    //根据时间和科目查询学生签到状态
//    String queryStuAttend(Date startTime, Long subjectId);

    List<AttendanceVo> queryStuAttendInfo(Integer status,  Long facultiesId, Date startTime, Long subjectId);



    //根据学生id查询缺勤总次数
    Long attCountByUserId(Long userId,Long subjectId);
}
