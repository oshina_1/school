package com.lzy.service.impl;

import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lzy.mapper.LessonMapper;
import com.lzy.pojo.Lesson;
import com.lzy.service.LessonService;
import org.springframework.stereotype.Service;

/**
* @author Administrator
* @description 针对表【lesson(课程表)】的数据库操作Service实现
* @createDate 2023-09-01 17:31:31
*/
@Service
public class LessonServiceImpl extends ServiceImpl<LessonMapper, Lesson>
    implements LessonService {

}




