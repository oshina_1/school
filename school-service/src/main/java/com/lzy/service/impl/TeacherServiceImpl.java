package com.lzy.service.impl;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lzy.mapper.TeacherMapper;
import com.lzy.mapper.UserMapper;
import com.lzy.pojo.*;
import com.lzy.service.*;
import com.lzy.vo.*;
import com.lzy.util.PageUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
* @author zyj
* @description 针对表【user】的数据库操作Service实现
* @createDate 2023-08-29 15:22:00
*/
@Service
public class TeacherServiceImpl implements TeacherService {

  @Autowired
  private AttendanceService attendanceService;
  @Autowired
  private SubjectService subjectService;
  @Autowired
  private UserSubjectService uss;
  @Autowired
  private EvaluateService evaluateService;
  @Autowired
  private TeacherMapper teacherMapper;
  @Autowired
  private ScoreService scoreService;


  @Override
  public List<UserSubjectVo> allTeaSubject(Long teaId) {
    return uss.allTeaSubject(teaId);
  }



  @Override
  public List<AttendanceVo> findAllStudentBySubId(Long subjectId) {
    return attendanceService.findAllStudentByTeaId(subjectId);
  }

  @Override
  public int updateStudentAttendanceByStuId(Integer status, Long studentId, Long subjectId) {
    // 使用LambdaUpdateWrapper创建更新条件
    LambdaUpdateWrapper<Attendance> updateWrapper = Wrappers.lambdaUpdate(Attendance.class)
            .eq(Attendance::getUserId, studentId)
            .eq(Attendance::getSubjectId,subjectId);

    // 设置要更新的字段和新的状态值
    updateWrapper.set(Attendance::getStatus, status);

    // 调用更新方法执行更新操作
    boolean updateResult = attendanceService.update(updateWrapper);

    if (updateResult) {
      System.out.println("成功更新学生ID为 " + studentId + " 的签到状态为 " + status);
      return 1; // 返回1表示成功更新了一个学生的状态
    } else {
      System.out.println("更新失败");
      return 0; // 返回0表示更新失败
    }
  }


  //查询所有课程
  @Override
  public List<Subject> allSubject(PageUtils pageUtils) {

    // 执行查询操作
    List<Subject> subjects = subjectService.selectSubjectVo(pageUtils.getCurrent(), pageUtils.getSize(), pageUtils.getQuery());

    return subjects;
  }

  @Override
  public List<Subject> allSubject() {
    LambdaQueryWrapper<Subject> queryWrapper= new LambdaQueryWrapper<>();
    // 执行查询操作
    List<Subject> subjects = subjectService.list(queryWrapper);

    return subjects;
  }


  @Override
  public String teaAddSubject(UserSubject userSubject) {

    return uss.findSubject(userSubject);

  }

  @Override
  public Long subCount(String name) {

    return subjectService.count(name);
  }


  //根据学生id查询缺勤总次数
  @Override
  public Long attCountByUserId(Long userId, Long subjectId) {
    return attendanceService.attCountByUserId(userId,subjectId);
  }


  @Override
  public List<AttendanceVo> queryStuAttendInfo(Integer status, Long facultiesId, Date startTime, Long subjectId) {

    List<AttendanceVo> attendanceVo = attendanceService.queryStuAttendInfo(status, facultiesId, startTime, subjectId);
    for (AttendanceVo att : attendanceVo) {
      att.setCount(this.attCountByUserId(att.getStudentId(),subjectId));
    }
    return attendanceVo;
  }

  @Override
  public String teaEvaTeaBySubId(Evaluate evaluate) {
    if (evaluateService.save(evaluate)) {
      return "添加成功";
    }
    return "添加失败";
  }

  @Override
  public List<Subject> listLikeSelectByName(String name) {
    return subjectService.listLikeSelectByName(name);
  }

  @Override
  public void classroomQuery(ClassroomPageVo classroomVo) {
    int count = teacherMapper.findCount(classroomVo.getClassroom());
    classroomVo.getPage().setCount(count);
    classroomVo.getPage().setIndex((classroomVo.getPage().getPageNo() - 1) * classroomVo.getPage().getPageSize());
    List<ClassroomVo> list = teacherMapper.findByPage(classroomVo);
    classroomVo.getPage().setData(list);
  }

  @Override
  public List<Faculties> selectFac() {
    return teacherMapper.selectFac();
  }

  @Override
  public List<EvaluateVo> findEvaBySubjectId(Long subjectId, Long roleId, Long facultiesId, Long userId, Long createBy) {
    return evaluateService.findEvaBySubjectId(subjectId,roleId,facultiesId,userId, createBy);
  }

  @Override
  public void selectStuBySub(StuPageVo stuPageVo) {
    int count = teacherMapper.selectCountBySub(stuPageVo.getStudentVo());
    stuPageVo.getPage().setCount(count);
    stuPageVo.getPage().setIndex((stuPageVo.getPage().getPageNo() - 1) * stuPageVo.getPage().getPageSize());
    List<StudentVo> list = teacherMapper.selectStuBySub(stuPageVo);
    stuPageVo.getPage().setData(list);
  }

  @Override
  public void selectStuScorePageByClassId(StuScorePageVo stuScorePageVo) {
    int count = teacherMapper.selectStuCountByClassId(stuScorePageVo.getStuScoreVo());
    stuScorePageVo.getPage().setCount(count);
    stuScorePageVo.getPage().setIndex((stuScorePageVo.getPage().getPageNo() - 1) * stuScorePageVo.getPage().getPageSize());
    List<StuScoreVo> list = teacherMapper.selectStuScorePageByClassId(stuScorePageVo);
    stuScorePageVo.getPage().setData(list);
  }

  @Override
  public List<ScoreInfoVo> selectScoreByFacAndUserId(Long subjectId, Long facultiesId) {
    return scoreService.selectScoreByFacAndUserId(subjectId,facultiesId);
  }

  @Override
  public void updateIsScore(Long userId, Long subjectId) {
    uss.updateIsScore(userId,subjectId);
  }
}


