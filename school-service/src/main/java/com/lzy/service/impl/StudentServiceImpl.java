package com.lzy.service.impl;


import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lzy.mapper.UserMapper;
import com.lzy.pojo.Score;
import com.lzy.pojo.User;
import com.lzy.pojo.UserSubject;
import com.lzy.service.ScoreService;
import com.lzy.service.StudentService;
import com.lzy.service.SubjectService;
import com.lzy.service.UserSubjectService;
import com.lzy.vo.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
* @author zyj
* @description 针对表【user】的数据库操作Service实现
* @createDate 2023-08-29 15:22:00
*/
@Service
public class StudentServiceImpl extends ServiceImpl<UserMapper, User>
implements StudentService {

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private UserSubjectService uss;

    @Autowired
    private SubjectService subjectService;
    @Autowired
    private ScoreService scoreService;


    @Override
    public StudentVo selectInfo(Long stuId) {
        StudentVo studentVo = userMapper.findById(stuId);
        return studentVo;
    }

    public boolean updateStudentInfo(StudentVo studentVo){
        LambdaUpdateWrapper<User> updateWrapper = new LambdaUpdateWrapper<>();
        //修改条件
        updateWrapper.eq(User::getId,studentVo.getId());
        if(studentVo.getName()!= null){
            updateWrapper.set(User::getName,studentVo.getName());
        }

        if(studentVo.getPhone() != null){
            updateWrapper.set(User::getPhone,studentVo.getPhone());
        }

        if(studentVo.getAddress() != null){
            updateWrapper.set(User::getAddress,studentVo.getAddress());
        }

        if(studentVo.getBirthday() != null){
            updateWrapper.set(User::getBirthday,studentVo.getBirthday());
        }

        if(studentVo.getEmail() != null){
            updateWrapper.set(User::getEmail,studentVo.getEmail());
        }

        //修该字段
        int row = userMapper.update(null,updateWrapper);
        return row > 0 ;
    }

    @Override
    public List<UserSubjectVo> allStuSubject(Long userId) {
        return uss.allStuSubject(userId);
    }


    //根据课程id找到对应课程的评价信息
   public List<SubjectVo> selectSubjectByUserId(Long facultiesId,Long userId){
        return subjectService.selectSubjectByUserId(facultiesId,userId);
   }

    @Override
    public String updateBySubjectId(Long subjectId) {
        if(uss.updateBySubjectId(subjectId)){
            return "更新成功";
        }
        return "更新失败";
    }

    @Override
    public List<ScoreVo> selectScoreByUserIdAndSemester(IPage<ScoreVo> page, Long userId, Long grateId) {
        return scoreService.selectScoreByUserIdAndSemester(page,userId,grateId);
    }

    @Override
    public List<ScoreInfo> selectGrateByUserIdr(Long userId) {
        return scoreService.selectGrateByUserIdr(userId);
    }


}
