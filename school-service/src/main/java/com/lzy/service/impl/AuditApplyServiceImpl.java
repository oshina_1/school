package com.lzy.service.impl;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.lzy.mapper.AuditApplyMapper;
import com.lzy.mapper.AuditDetailMapper;
import com.lzy.pojo.AuditApply;
import com.lzy.pojo.AuditDetail;
import com.lzy.pojo.AuditDiagram;
import com.lzy.service.AuditApplyService;
import com.lzy.service.AuditDetailService;
import com.lzy.service.AuditDiagramService;
import com.lzy.vo.AuditApplyVo;
import com.lzy.vo.AuditDetailVo;
import com.lzy.vo.AuditNode;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
public class AuditApplyServiceImpl extends ServiceImpl<AuditApplyMapper, AuditApply> implements
        AuditApplyService {

    @Autowired
    private AuditDetailMapper detailMapper;
    @Autowired
    private AuditDiagramService auditDiagramService;
    @Autowired
    private AuditDetailService detailService;
    @Autowired
    private AuditApplyMapper auditApplyMapper;
    @Override
    public void apply(AuditApply auditApply) throws JsonProcessingException {
        // 判断当前的申请是什么类型 -> 选择对应的流程图
        AuditDiagram diagram = null;
        switch (auditApply.getApplyType()) {
            case 1:
                QueryWrapper<AuditDiagram> queryWrapper = new QueryWrapper<>();
                queryWrapper.lambda().eq(AuditDiagram::getAuditName, "1系认定申请");
                // 查询请假的流程
                diagram = auditDiagramService.getOne(queryWrapper);
                break;
            case 2:
                QueryWrapper<AuditDiagram> queryWrapper2 = new QueryWrapper<>();
                queryWrapper2.lambda().eq(AuditDiagram::getAuditName, "2系认定申请");
                // 查询请假的流程
                diagram = auditDiagramService.getOne(queryWrapper2);
                break;
            case 3:
                QueryWrapper<AuditDiagram> queryWrapper3 = new QueryWrapper<>();
                queryWrapper3.lambda().eq(AuditDiagram::getAuditName, "3系认定申请");
                // 查询请假的流程
                diagram = auditDiagramService.getOne(queryWrapper3);
                break;
            case 4:
                QueryWrapper<AuditDiagram> queryWrapper4 = new QueryWrapper<>();
                queryWrapper4.lambda().eq(AuditDiagram::getAuditName, "4系认定申请");
                // 查询请假的流程
                diagram = auditDiagramService.getOne(queryWrapper4);
                break;
        }
        if (diagram != null) {
            String auditNodes = diagram.getAuditNodes();
            // 将节点转成对象
            ObjectMapper objectMapper = new ObjectMapper();
            List<AuditNode> list = objectMapper.readValue(auditNodes, new TypeReference<List<AuditNode>>() {});
            // 获取第一个节点
            AuditNode auditNode = list.get(0);
            // 保存申请数据
            auditApply.setApplyTime(LocalDateTime.now());
            boolean save = this.save(auditApply);
            // 封装申请记录对象
            AuditDetail auditDetail = new AuditDetail();
            auditDetail.setApplyId(auditApply.getId());
            auditDetail.setAuditorId(auditNode.getAuditorId());
            auditDetail.setDiagramId(diagram.getId());
            auditDetail.setCreateTime(LocalDateTime.now());
            auditDetail.setAuditStatus(0L);
            // 添加一条记录
            boolean save1 = detailService.save(auditDetail);
            // TODO 通知用户
        } else {
            log.error("找不到流程图");
        }
    }

    @Override
    public List<AuditApply> findAuditList(long userId) {
        return auditApplyMapper.findAuditList(userId);
    }

    @Override
    public void audit(AuditDetail detail) {
        detail.setAuditTime(LocalDateTime.now());
        //这部分代码有漏洞但是能运行
        if(detail.getAuditStatus() == 1){
            //申请通过 -> 修改申请数据状态
            auditApplyMapper.updateStatusByApplyId
                    (detail.getApplyId(), 1);
            detailMapper.updateByApplyId(detail.getApplyId(), 1);
            detailMapper.updateAuditTime(detail.getApplyId(), detail.getAuditTime());
            detailMapper.updateRemark(detail.getApplyId(), detail.getRemark());
        } else if (detail.getAuditStatus() == -1) {
            //申请失败
            auditApplyMapper.updateStatusByApplyId
                    (detail.getApplyId(), -1);
            detailMapper.updateByApplyId(detail.getApplyId(), -1);
            detailMapper.updateAuditTime(detail.getApplyId(), detail.getAuditTime());
            detailMapper.updateRemark(detail.getApplyId(), detail.getRemark());
        }
//        正确代码审核流程只能拒绝同意会报错
//        //慎重判断if或其他选择结构可能出现的情况，以及处理方式
//        if(detail.getAuditStatus() == 1){
//            //通过流程图查询所有节点
//            QueryWrapper<AuditDiagram> d = new QueryWrapper<>();
//            d.lambda().eq(AuditDiagram::getId,detail.getDiagramId());
//            AuditDiagram one = auditDiagramService.getOne(d);
//
//            String auditNode = one.getAuditNodes();
//            ObjectMapper objectMapper = new ObjectMapper();
//            List<AuditNode> auditNodes = objectMapper.readValue(auditNode,new TypeReference<List<AuditNode>>() {});
//
//            //判断有没有下一个节点
//            List<AuditNode> list = auditNodes
//                    .stream()
//                    //只保留当前审核节点后的数据
//                    .filter(n -> n.getId() > detail.getNodeId())
//                    .toList();
//            //添加新的审核记录
//            if(!list.isEmpty()){
//                //添加新的审核
//                AuditDetail auditDetail = new AuditDetail();
//                //设置下一个节点id
//                auditDetail.setNodeId(list.get(0).getId());
//                auditDetail.setApplyId(detail.getApplyId());
//                auditDetail.setDiagramId(detail.getDiagramId());
//                auditDetail.setAuditorId(list.get(0).getAuditorId());
//                auditDetail.setAuditTime(LocalDateTime.now());
//                boolean save = detailService.save(auditDetail);
//            } else {
//                //申请通过 -> 修改申请数据状态
//                auditApplyMapper.updateStatusByApplyId
//                        (detail.getApplyId(), 1);
//            }
//        } else if (detail.getAuditStatus() == -1){
//            //申请失败
//            detailMapper.updateByApplyId(detail.getApplyId(), -1);
//            auditApplyMapper.updateStatusByApplyId
//                    (detail.getApplyId(), -1);
//        }else {
//            log.error("错误的审核数据");
//        }
//        detailService.updateById(detail);
    }

    @Override
    public List<AuditApplyVo> degree(IPage<AuditApplyVo> page, long userId) {
        //        //查询申请的所有审批记录
//        QueryWrapper<AuditDetail> d = new QueryWrapper<>();
//        d.lambda().eq(AuditDetail::getApplyId,userId);
//        List<AuditDetail> auditDetails = detailService.list(d);
//        long diagramId = auditDetails.get(0).getDiagramId();
////        查询所有节点
//        QueryWrapper<AuditDiagram> condition = new QueryWrapper<>();
//        condition.lambda().eq(AuditDiagram::getId,diagramId);
//        AuditDiagram diagrams = auditDiagramService.getOne(condition);
//        String auditNode = diagrams.getAuditNodes();
//        ObjectMapper objectMapper = new ObjectMapper();
//        List<AuditNode> allNodes = objectMapper.readValue(auditNode,new TypeReference<List<AuditNode>>() {});
//        //查询申请信息
//        QueryWrapper<AuditApply> applyCondition = new QueryWrapper<>();
//        applyCondition.lambda().eq(AuditApply::getId,userId);
//        AuditApply apply = this.getOne(applyCondition);
//        return new AuditApplyVo(userId, apply.getTitle(), apply.getApplyType(), allNodes, auditDetails);
        return auditApplyMapper.degree(page, userId);
    }

    @Override
    public List<AuditApplyVo> degreeTwo(Long userId) {
        return auditApplyMapper.degreeTwo(userId);
    }

//    @Override
//    public int applyNotice(long userId) {
//        return auditApplyMapper.applyNotice(userId);
//    }

    @Override
    public boolean cancel(long id) {
//        int i = 0;
//        //查询审批记录
//        QueryWrapper<AuditDetail> d = new QueryWrapper<>();
//        ArrayList<Integer> status = new ArrayList<>();
//        status.add(1);
//        status.add(-1);
//        d.lambda()
//                .eq(AuditDetail::getApplyId,applyId)
//                .in(AuditDetail::getAuditStatus, status);
//        List<AuditDetail> auditDetails = detailService.list(d);
//
//        //判断有没有被审批过
//        if (!auditDetails.isEmpty()) {
//            //取消 修改申请数据的状态
//            i = auditApplyMapper.updateStatusByApplyId(applyId, 3);
//        }else {
//            log.error("已被审批，无法撤销");
//        }
//        return i > 0;
        return auditApplyMapper.cancel(id);
    }


}
