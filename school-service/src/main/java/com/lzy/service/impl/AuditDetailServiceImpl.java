package com.lzy.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lzy.mapper.AuditDetailMapper;
import com.lzy.pojo.AuditDetail;
import com.lzy.service.AuditDetailService;
import com.lzy.vo.AuditDetailVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AuditDetailServiceImpl extends ServiceImpl<AuditDetailMapper, AuditDetail> implements AuditDetailService {

    @Autowired
    private AuditDetailMapper detailMapper;
    @Override
    public List<AuditDetailVo> findDetailsByApplyId(IPage<AuditDetailVo> page, Long auditorId) {
        return detailMapper.findDetailByAuditorId(page,auditorId);
//        QueryWrapper<AuditDetail> condition = new QueryWrapper();
//        condition.lambda().eq(AuditDetail::getApplyId, applyId);
//        List<AuditDetail> list = this.list(condition);
//        return list;
    }

    @Override
    public int auditNotice(long userId) {
        return detailMapper.auditNotice(userId);
    }
}
