package com.lzy.service.impl;

import com.lzy.mapper.RoleMapper;
import com.lzy.pojo.Role;
import com.lzy.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;

@Service
public class RoleServiceImpl implements RoleService {
    @Autowired
    private RoleMapper roleMapper;

    @Override
    public List<Role> findAllByUserId(long id) {
        return roleMapper.findAllByUserId(id);
    }
}
