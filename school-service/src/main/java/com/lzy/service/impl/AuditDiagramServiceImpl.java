package com.lzy.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lzy.mapper.AuditDiagramMapper;
import com.lzy.pojo.AuditDiagram;
import com.lzy.pojo.User;
import com.lzy.service.AuditDiagramService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AuditDiagramServiceImpl extends ServiceImpl<AuditDiagramMapper, AuditDiagram> implements AuditDiagramService {

    @Autowired
    private AuditDiagramMapper auditDiagramMapper;

    @Override
    public List<User> selectTea() {
        return auditDiagramMapper.selectTea();
    }
}
