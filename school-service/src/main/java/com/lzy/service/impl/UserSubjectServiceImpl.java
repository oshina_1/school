package com.lzy.service.impl;

import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lzy.mapper.UserSubjectMapper;
import com.lzy.pojo.UserSubject;
import com.lzy.service.UserSubjectService;
import com.lzy.vo.UserSubjectVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
* @author Administrator
* @description 针对表【user_subject(学生选课表)】的数据库操作Service实现
* @createDate 2023-09-02 11:05:54
*/
@Service
public class UserSubjectServiceImpl extends ServiceImpl<UserSubjectMapper, UserSubject>
    implements UserSubjectService {

    @Autowired
    private UserSubjectMapper userSubjectMapper;

    @Override
    public List<UserSubjectVo> allTeaSubject(Long teaId) {
        return userSubjectMapper.teaSubject(teaId);
    }

    //学生所有课程
    @Override
    public List<UserSubjectVo> allStuSubject(Long userId) {
        return userSubjectMapper.stuSubject(userId);
    }

    @Override
    public String  findSubject(UserSubject userSubject) {
        //根据科目名来找到学科信息
        List<UserSubjectVo> us = this.allTeaSubject(userSubject.getUserId());
        //如果教师安排的课程超过3，则无法选择课程
        if(us.size() < 3){
            boolean hasDuplicate = false;
            for (UserSubjectVo u : us) {
                if (u.getTeaSubjectId().equals(userSubject.getTeaSubjectId())) {
                    hasDuplicate = true;
                    break; // 找到重复的 subjectId，退出循环
                }
            }

            if (!hasDuplicate) {
                this.save(userSubject);
                return "添加成功";
            } else {
                return "不能添加相同课程";
            }
        }

        return "课程超过3门，无法选择";
    }

    @Override
    public boolean updateBySubjectId(Long subjectId) {
        LambdaUpdateWrapper<UserSubject> updateWrapper = new LambdaUpdateWrapper<>();
        updateWrapper.eq(UserSubject::getSubjectId,subjectId);
        updateWrapper.set(UserSubject::getIsEvaluate,1);


        return this.update(updateWrapper);
    }

    @Override
    public void deleteByIds(List<Long> ids) {
        userSubjectMapper.deleteByIds(
                ids
        );
    }

    @Override
    public void updateIsScore(Long userId, Long subjectId) {
        LambdaUpdateWrapper<UserSubject> updateWrapper = new LambdaUpdateWrapper<>();
        updateWrapper.eq(UserSubject::getSubjectId,subjectId)
                .eq(UserSubject::getUserId,userId)
                .set(UserSubject::getIsScore,1);
        userSubjectMapper.update(null,updateWrapper);
    }
}




