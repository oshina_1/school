package com.lzy.service.impl;

import com.lzy.mapper.TeaMapper;
import com.lzy.pojo.User;
import com.lzy.service.EvaluateService;
import com.lzy.service.TeaService;
import com.lzy.vo.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TeaServiceImpl implements TeaService {

    @Autowired
    private TeaMapper teaMapper;

    @Autowired
    private EvaluateService evaluateService;



    @Override
    public List<EvaluateVo> findTeaEvaBySubjectId(String subjectName, Long roleId, String teacherName,
                                                  Long facultiesId) {
        return evaluateService.findTeaEvaBySubjectId(subjectName, roleId, teacherName, facultiesId);
    }

    @Override
    public void getStuScore(StuScorePageVo stuScorePageVo) {

        int count = teaMapper.findCount(stuScorePageVo.getStuScoreVo());
        stuScorePageVo.getPage().setCount(count);
        stuScorePageVo.getPage().setIndex((stuScorePageVo.getPage().getPageNo() - 1) * stuScorePageVo.getPage().getPageSize());
        List<StuScoreVo> list = teaMapper.findByPage(stuScorePageVo);
        stuScorePageVo.getPage().setData(list);
    }

    @Override
    public void findExamination(ExaminationPageVo examinationPageVo) {
        int count = teaMapper.findExaminationCount(examinationPageVo.getExaminationVo());
        examinationPageVo.getPage().setCount(count);
        examinationPageVo.getPage().setIndex((examinationPageVo.getPage().getPageNo() - 1) * examinationPageVo.getPage().getPageSize());
        List<ExaminationVo> list = teaMapper.findExamination(examinationPageVo);
        examinationPageVo.getPage().setData(list);
    }

    @Override
    public List<ExaminationVo> selectPaper(Long facultiesId) {
        return teaMapper.selectPaper(facultiesId);
    }

    @Override
    public List<ExaminationVo> selectClassroom() {
        return teaMapper.selectClassroom();
    }

    @Override
    public List<ExaminationVo> selectSubject(Long facultiesId) {
        return teaMapper.selectSubject(facultiesId);
    }

    @Override
    public int deleteInv(Long id) {
        return teaMapper.deleteInv(id);
    }

    @Override
    public List<ExaminationVo> selectLead(Long facultiesId) {
        return teaMapper.selectLead(facultiesId);
    }

    @Override
    public List<ExaminationVo> selectAssociate(Long facultiesId) {
        return teaMapper.selectAssociate(facultiesId);
    }

    @Override
    public int updateInv(ExaminationVo examinationVo) {
        return teaMapper.updateInv(examinationVo);
    }

    @Override
    public int insertInv(ExaminationVo examinationVo) {
        int testNumber = teaMapper.stuCount(examinationVo.getPId());
        examinationVo.setTestNumber(testNumber);
        return teaMapper.insertInv(examinationVo);
    }

    @Override
    public void findPaper(PaperPageVo paperPageVo) {
        int count = teaMapper.findPaperCount(paperPageVo.getPaperVo());
        paperPageVo.getPage().setCount(count);
        paperPageVo.getPage().setIndex((paperPageVo.getPage().getPageNo() - 1) * paperPageVo.getPage().getPageSize());
        List<PaperVo> list = teaMapper.findPaper(paperPageVo);
        paperPageVo.getPage().setData(list);
    }

    @Override
    public int deletePaper(Long id) {
        return teaMapper.deletePaper(id);
    }

    @Override
    public int insertPaper(PaperVo paperVo) {
        return teaMapper.insertPaper(paperVo);
    }

    @Override
    public int updatePaper(PaperVo paperVo) {
        return teaMapper.updatePaper(paperVo);
    }

    @Override
    public void findClassroom(ClassroomPageVo classroomPageVo) {
        int count = teaMapper.findClassroomCount(classroomPageVo.getClassroom());
        classroomPageVo.getPage().setCount(count);
        classroomPageVo.getPage().setIndex((classroomPageVo.getPage().getPageNo() - 1) * classroomPageVo.getPage().getPageSize());
        List<ClassroomVo> list = teaMapper.findClassroom(classroomPageVo);
        classroomPageVo.getPage().setData(list);
    }

    @Override
    public void selectUser(UserPageVo userPageVo) {
        int count = teaMapper.findUserCount(userPageVo.getUser());
        userPageVo.getPage().setCount(count);
        userPageVo.getPage().setIndex((userPageVo.getPage().getPageNo() - 1) * userPageVo.getPage().getPageSize());
        List<User> list = teaMapper.selectUser(userPageVo);
        userPageVo.getPage().setData(list);
    }
}
