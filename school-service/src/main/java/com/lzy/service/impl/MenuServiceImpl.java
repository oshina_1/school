package com.lzy.service.impl;

import com.lzy.mapper.MenuMapper;
import com.lzy.pojo.Menu;
import com.lzy.service.MenuService;
import com.lzy.vo.MetaVo;
import com.lzy.vo.RouterVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static java.util.stream.Collectors.toList;

@Service
public class MenuServiceImpl implements MenuService {

    @Autowired
    private MenuMapper menuMapper;

    public List<Menu> findMenuTreeByUserId(long id) {
        // 查询时 不查按钮
        List<Menu> menus = menuMapper.findMenuByUserId(id);
        return getChildren(menus, 0L);
    }

    public List<Menu> getChildren(List<Menu> permissions, Long parentId) {
        // 找出parentId为0的权限  jdk8的特性 stream流
        List<Menu> root = permissions.stream()
                .filter(m -> m.getParentId() == parentId) // 先过滤出四条数据
                .collect(toList());
        // root只有四条数据
        root.forEach(m -> {
            List<Menu> childs = getChildren(permissions, m.getMenuId());
            m.setChildren(childs);
        });
        return root;
    }

    public List<RouterVo> buildRouters(List<Menu> permissions) {
        List<RouterVo> routers = new ArrayList<>();
        // 将permission对象的集合变成路由增强类的集合
        permissions.forEach(menu -> {
            RouterVo router = new RouterVo();
            // 将p对象的属性往RouterVo中设置
            router.setHidden("1".equals(menu.getVisible()));
            router.setName(menu.getMenuName());
            router.setPath(menu.getPath());
            router.setComponent(menu.getComponent());
            router.setQuery(menu.getQuery());
            router.setMeta((new MetaVo(menu.getMenuName(), menu.getIcon(), false, menu.getPath())));
            // 判断有没有子节点
            if (menu.getChildren().size() > 0) {
                List<RouterVo> childs = buildRouters(menu.getChildren());
                router.setChildren(childs);
            }
            routers.add(router);
        });
        // 最后将结果的集合返回给前端
        return routers;
    }

    @Override
    public List<Menu> findAllByUserId(long id) {
        return menuMapper.findAllByUserId(id);
    }


}
