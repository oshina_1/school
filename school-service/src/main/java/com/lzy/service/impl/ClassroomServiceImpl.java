package com.lzy.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import com.lzy.mapper.ClassroomMapper;
import com.lzy.pojo.Classroom;
import com.lzy.service.ClassroomService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import static net.sf.jsqlparser.parser.feature.Feature.insert;

/**
* @author 24846
* @description 针对表【classroom】的数据库操作Service实现
* @createDate 2023-09-01 14:44:35
*/
@Service
public class ClassroomServiceImpl extends ServiceImpl<ClassroomMapper, Classroom> implements ClassroomService {
    @Autowired
    private ClassroomMapper classroomMapper;
    @Override
    public void insertRoom(Classroom classroom) {
        List<Classroom> list = classroomMapper.selectList(null);
        for (Classroom  c: list) {
            if (c.getName().equals(classroom.getName())) {
                System.out.println("教室名重复");
                break;
            }
        }
        classroomMapper.insert(classroom);
    }
    @Override
    public List<Classroom> selectRoom(Classroom classroom) {
        List<Classroom> classrooms = classroomMapper.selectList(null);
        return classrooms;
    }

    @Override
    public Page<Classroom> getClassPage(String id,Integer currenter, Integer pageSizer) {
        QueryWrapper<Classroom> queryWrapper = new QueryWrapper<>();
        if (id != null){
            queryWrapper.like("id",id);
        }
        Page<Classroom> page = new Page<>(currenter,pageSizer);
         classroomMapper.selectPage(page, queryWrapper);
        return page;
    }

    @Override
    public boolean insertClass(Classroom classroom) {
        Classroom byId = classroomMapper.selectById(classroom.getId());
        if (byId != null){
            System.out.println("教室已存在");
            return false;
        }else {
            int insert = classroomMapper.insert(classroom);
            return insert>0;
        }


    }
    @Override
    public boolean deleteClass(Long id) {
        if (id == null){
            System.out.println("此教室不存在");
        }
        int i = classroomMapper.deleteById(id);
        return i>0;
    }
}




