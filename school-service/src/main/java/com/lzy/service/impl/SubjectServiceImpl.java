package com.lzy.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lzy.mapper.SubjectMapper;
import com.lzy.pojo.Subject;
import com.lzy.service.SubjectService;
import com.lzy.vo.SubjectVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
* @author Administrator
* @description 针对表【subject(学科表)】的数据库操作Service实现
* @createDate 2023-09-01 13:55:06
*/
@Service
public class SubjectServiceImpl extends ServiceImpl<SubjectMapper, Subject>
    implements SubjectService {
    @Autowired
    private SubjectMapper subjectMapper;

    @Override
    public List<Subject> selectSubjectVo(int current, int size, String name) {
        Page<Subject> page = new Page<>(current,size);
        return subjectMapper.selectSubjectVo(page, name);
    }

    @Override
    public Long count(String name) {
        LambdaQueryWrapper<Subject> queryWrapper = Wrappers.lambdaQuery();
        // 添加查询条件
        queryWrapper.like(Subject::getName, name); // 示例条件：根据名称模糊查询

        // 调用 count 方法获取符合条件的数据总数

        return subjectMapper.selectCount(queryWrapper);
    }

    @Override
    public List<Subject> listLikeSelectByName(String name) {
        LambdaQueryWrapper<Subject> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.like(Subject::getName,name);
        return this.list(queryWrapper);

    }

    @Override
    public List<SubjectVo> selectSubjectByUserId(Long facultiesId,Long userId) {
        return subjectMapper.selectSubjectByUserId(facultiesId,userId);
    }


}




