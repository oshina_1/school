package com.lzy.service.impl;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lzy.mapper.UserMapper;
import com.lzy.mapper.UserRoleMapper;
import com.lzy.pojo.User;
import com.lzy.service.UserService;
import com.lzy.util.Md5Utils;
import com.lzy.vo.UpdateUserVo;
import com.lzy.vo.UserVo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

/**
* @author zyj
* @description 针对表【user】的数据库操作Service实现
* @createDate 2023-08-29 15:22:00
*/
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User>
implements UserService {

    @Autowired
    private UserMapper userMapper;
    @Autowired
    private UserRoleMapper userRoleMapper;
    //获取系部id -》 根据当前用户id
    @Override
    public Long findfacultiesIdByUserId(Long userId) {
        User user = userMapper.selectById(userId);
        return user.getFacultiesId();
    }

    @Override
    public String findName(Long id) {
        return userMapper.findName(id);
    }

    @Override
    public User findUser(Long id) {
        User user = userMapper.selectById(id);
        return user;
    }

    @Override
    public void updatePassword(Long id, String newpassword) {
        User user = userMapper.selectById(id);
        if (user != null) {
            user.setPassword(newpassword);
            userMapper.updateById(user);
        }
    }
    @Override
    @Transactional
    public Page<User> getUserPage(@RequestBody User user1, Integer current, Integer pageSize) {
        if (current == null || pageSize == null || current < 1 || pageSize < 1) {
            throw new IllegalArgumentException("Invalid input parameters");
        }
        LambdaQueryWrapper<User> queryWrapper = new LambdaQueryWrapper<>();
        Page<User> page = new Page<>(current,pageSize);
        if (user1.getId() != null){
            queryWrapper.eq(User::getId,user1.getId());
        } else if (StringUtils.isNotBlank(user1.getName())) {
            queryWrapper.like(User::getName,user1.getName());
        }
        Page<User> page1 = userMapper.selectPage(page, queryWrapper);
        return  page1;
    }
//    @Override
//    public List<User> selectUser(User user) {
//        List<User> users = userMapper.selectUser(user);
//        if (user != null){
//            userMapper.selectUser(user);
//        }
//        return users;
//    }

    @Override
    public int insert(User user,Long roleId) {
        List<User> users = userMapper.selectList(null);
//        users.forEach(user1 -> {
//            if ()
//        });
        for (User u:users) {
            if (u.getCardId().equals(user.getCardId())){
                System.out.println("身份证号重复，请重新出入");
            }
            break;
        }
        if (user.getPassword() != null){
            String hash = Md5Utils.hash(user.getPassword());
            user.setPassword(hash);
        }
        int i = userMapper.insert(user);
        Long userId = user.getId();
        userRoleMapper.insertAll(userId,roleId);
        return i;
    }

    @Override
    public boolean deleteId(Long id) {
        int i = userMapper.deleteById(id);
        return i>0;
    }
    @Override
    public boolean updateUser(UpdateUserVo updateUserVo) {
        boolean update = userMapper.update(updateUserVo);
        return update;
    }

    @Override
    public Long findFacultiesId(Long id) {
        return userMapper.findFacultiesId(id);
    }

    @Override
    public List<User> findAllTeachers() {
        return userMapper.findAllTeachers();
    }
}
