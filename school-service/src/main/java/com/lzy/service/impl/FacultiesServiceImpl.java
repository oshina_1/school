package com.lzy.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lzy.mapper.FacultiesMapper;
import com.lzy.pojo.Faculties;
import com.lzy.service.FacultiesService;
import org.springframework.stereotype.Service;

/**
* @author zyj
* @description 针对表【faculties(院系表)】的数据库操作Service实现
* @createDate 2023-09-12 13:50:27
*/
@Service
public class FacultiesServiceImpl extends ServiceImpl<FacultiesMapper, Faculties>
    implements FacultiesService {

}




