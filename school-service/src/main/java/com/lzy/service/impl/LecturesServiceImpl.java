package com.lzy.service.impl;

import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lzy.mapper.LecturesMapper;
import com.lzy.pojo.Lectures;
import com.lzy.service.LecturesService;
import com.lzy.vo.LecturesVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
* @author Administrator
* @description 针对表【lectures(听课表)】的数据库操作Service实现
* @createDate 2023-09-02 11:29:34
*/
@Service
public class LecturesServiceImpl extends ServiceImpl<LecturesMapper, Lectures>
    implements LecturesService {
    @Autowired
    private LecturesMapper lecturesMapper;
    @Override
    public List<LecturesVo> myLectures(Long userId) {

        return lecturesMapper.myLectures(userId);
    }

    @Override
    public boolean updateLectures(Long id) {
        LambdaUpdateWrapper<Lectures> updateWrapper = new LambdaUpdateWrapper<>();
        // 设置更新条件，这里假设要根据 lectureId 进行更新
        updateWrapper.eq(Lectures::getId, id);

        // 设置要更新的字段和值
        updateWrapper.set(Lectures::getIsEvaluate, 1);

        // 执行更新操作
        int updatedRows = lecturesMapper.update(null, updateWrapper);

        if (updatedRows > 0) {
            System.out.println("更新成功！");
            return true;
        }
            System.out.println("更新失败或没有匹配的记录。");
        return false;
    }
}




