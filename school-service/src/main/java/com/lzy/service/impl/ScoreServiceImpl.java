package com.lzy.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lzy.pojo.Score;
import com.lzy.service.ScoreService;
import com.lzy.mapper.ScoreMapper;
import com.lzy.vo.ScoreInfo;
import com.lzy.vo.ScoreInfoVo;
import com.lzy.vo.ScoreVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
* @author Administrator
* @description 针对表【score(分数表)】的数据库操作Service实现
* @createDate 2023-09-14 11:04:54
*/
@Service
public class ScoreServiceImpl extends ServiceImpl<ScoreMapper, Score>
    implements ScoreService{

    @Autowired
    private ScoreMapper scoreMapper;
    @Override
    public List<ScoreVo> selectScoreByUserIdAndSemester(IPage<ScoreVo> page, Long userId, Long grateId) {
        List<ScoreVo> scoreVos = scoreMapper.selectScoreByUserIdAndSemester(page,userId,grateId);
        for (ScoreVo score: scoreVos) {
            score.setSumScore(score.getScoreDaily(),score.getScore());
        }
        return scoreVos;
    }

    @Override
    public List<ScoreInfo> selectGrateByUserIdr(Long userId) {
        LambdaQueryWrapper<Score> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(Score::getUserId, userId);
        List<Score> list = this.list(queryWrapper);
        List<ScoreInfo> lists = list.stream()
                .map(score -> new ScoreInfo(score.getGrateId()))
                .distinct() // 去除重复值
                .collect(Collectors.toList());
        return lists;

    }

    @Override
    public List<ScoreInfoVo> selectScoreByFacAndUserId(Long subjectId, Long facultiesId) {
        return scoreMapper.selectScoreByFacAndUserId(subjectId,facultiesId);
    }
}




