package com.lzy.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import com.lzy.mapper.NoticeMapper;
import com.lzy.pojo.Notice;
import com.lzy.result.Result;
import com.lzy.service.NoticeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
* @author 24846
* @description 针对表【notice(通知公告表)】的数据库操作Service实现
* @createDate 2023-09-01 14:44:35
*/
@Service
public class NoticeServiceImpl extends ServiceImpl<NoticeMapper, Notice>implements NoticeService {
    @Autowired
    private NoticeMapper noticeMapper;
    @Override
    public Page<Notice> selectAll(Notice notice,Integer current, Integer pageSize) {
        Page<Notice> page = new Page<>(current,pageSize);
        QueryWrapper<Notice> queryWrapper = new QueryWrapper<>();
        queryWrapper.orderByDesc("id");
//        List<Notice> notices = noticeMapper.selectList(queryWrapper);
        Page<Notice> page1 = noticeMapper.selectPage(page, queryWrapper);
        return page1;
    }

    @Override
    public Notice findById(Long id) {
        Notice notice = noticeMapper.selectById(id);
        return notice;
    }

    @Override
    public void insert(Notice notice) {
        List<Notice> list = noticeMapper.selectList(null);
        for (Notice n:list) {
            if (n.getId().equals(notice.getId())){
                System.out.println("通告已存在");
                break;
            }
        }
        noticeMapper.insert(notice);
    }

    @Override
    public Page<Notice> getUserPage(int pageNUm, int pageSize) {
        Page<Notice> page = new Page<>(pageNUm,pageSize);
        Page<Notice> page1 = noticeMapper.selectPage(page, null);
        return page1;
    }

    @Override
    public boolean delete(Long id) {
        int i = noticeMapper.deleteById(id);
        return i>0;
    }

    @Override
    public List<Notice> findNotice() {
        return noticeMapper.findNotice();
    }

    @Override
    public List<Notice> findNotify() {
        return noticeMapper.findNotify();
    }
}




