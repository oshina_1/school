package com.lzy.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lzy.mapper.AttendanceMapper;
import com.lzy.pojo.Attendance;
import com.lzy.service.AttendanceService;
import com.lzy.vo.AttendanceVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.DecimalFormat;
import java.util.Date;
import java.util.List;

/**
* @author Administrator
* @description 针对表【subject(学科表)】的数据库操作Service实现
* @createDate 2023-09-01 13:55:06
*/
@Service
public class AttendanceServiceImpl extends ServiceImpl<AttendanceMapper, Attendance>
    implements AttendanceService {

    @Autowired
    private AttendanceMapper attendanceMapper;


    //根据学科id获取学生签到信息
    @Override
    public List<AttendanceVo> findAllStudentByTeaId(Long subjectId) {
        return attendanceMapper.allStudentByTeacherId(subjectId);
    }

//    public String stuAttend() {
//        // 创建 LambdaQueryWrapper 对象
//        LambdaQueryWrapper<Attendance> queryWrapper = new LambdaQueryWrapper<>();
//        long count1 = attendanceMapper.selectCount(queryWrapper);
//        // 设置查询条件，这里以status字段为例
//        queryWrapper.eq(Attendance::getStatus, 0);
//
//        // 使用 count() 方法统计符合条件的记录数
//        long count2 = attendanceMapper.selectCount(queryWrapper);
//
//        double percentage = ((double) count2 / count1) * 100.0;
//
//        // 使用DecimalFormat格式化百分率，保留一位小数
//        DecimalFormat decimalFormat = new DecimalFormat("0.0");
//        String formattedPercentage = decimalFormat.format(percentage);
//
//        return formattedPercentage + "%";
//    }

//    @Override
//    public String  queryStuAttend(Date startTime, Long subjectId) {
//        //根据上课时间和学科id获取所有签到状态为0的学生数量
//        Integer i = 0;
//        int count = attendanceMapper.queryStuAttend(i,startTime,subjectId);
//        //获取所有签到表所有学生数量
//
//        long count1 = attendanceMapper.queryStuAttend(null,startTime,subjectId);
//        double percentage = ((double) count1 / count) * 100.0;
//
//        // 使用DecimalFormat格式化百分率，保留一位小数
//        DecimalFormat decimalFormat = new DecimalFormat("0.0");
//        String formattedPercentage = decimalFormat.format(percentage);
//        String attend = formattedPercentage + "%"; //出勤率
//        return "缺勤"+ (count1-count) + "人,出勤率为：" + attend;
//    }

    @Override
    public List<AttendanceVo> queryStuAttendInfo(Integer status, Long facultiesId,Date startTime, Long subjectId) {
        return attendanceMapper.queryStuAttendInfo(status,facultiesId,startTime,subjectId);
    }

    @Override
    public Long attCountByUserId(Long userId,Long subjectId) {
        LambdaQueryWrapper<Attendance> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.ne(Attendance::getStatus, 0)
                .eq(Attendance::getUserId, userId)
                .eq(Attendance::getSubjectId,subjectId);

        return attendanceMapper.selectCount(queryWrapper);
    }
}




