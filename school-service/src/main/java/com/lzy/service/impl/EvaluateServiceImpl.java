package com.lzy.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lzy.pojo.Evaluate;
import com.lzy.service.EvaluateService;
import com.lzy.mapper.EvaluateMapper;
import com.lzy.vo.EvaluateVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
* @author Administrator
* @description 针对表【evaluate(教师评价)】的数据库操作Service实现
* @createDate 2023-09-07 15:14:21
*/
@Service
public class EvaluateServiceImpl extends ServiceImpl<EvaluateMapper, Evaluate>
    implements EvaluateService{

    @Autowired
    private EvaluateMapper evaluateMapper;
    @Override
    public List<EvaluateVo> findTeaEvaBySubjectId(String subjectName, Long roleId, String teacherName,
                                                  Long facultiesId){
       return evaluateMapper.findTeaEvaBySubjectId(subjectName,roleId,teacherName,facultiesId);
    }

    @Override
    public List<EvaluateVo> findEvaBySubjectId(Long subjectId, Long roleId, Long facultiesId, Long userId,Long createBy) {
        return evaluateMapper.findEvaBySubjectId(subjectId,roleId,facultiesId,userId,createBy);
    }
}




