package com.lzy.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lzy.pojo.Lesson;

/**
* @author Administrator
* @description 针对表【lesson(课程表)】的数据库操作Service
* @createDate 2023-09-01 17:31:31
*/
public interface LessonService extends IService<Lesson> {

}
