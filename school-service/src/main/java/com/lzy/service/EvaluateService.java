package com.lzy.service;

import com.lzy.pojo.Evaluate;
import com.baomidou.mybatisplus.extension.service.IService;
import com.lzy.vo.EvaluateVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
* @author Administrator
* @description 针对表【evaluate(教师评价)】的数据库操作Service
* @createDate 2023-09-07 15:14:21
*/
public interface EvaluateService extends IService<Evaluate> {

    List<EvaluateVo> findTeaEvaBySubjectId(String subjectName,Long roleId,String teacherName, Long facultiesId);

    List<EvaluateVo> findEvaBySubjectId(Long subjectId,Long roleId, Long facultiesId,Long userId,Long createBy);
}
