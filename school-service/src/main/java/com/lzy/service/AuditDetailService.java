package com.lzy.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.lzy.pojo.AuditDetail;
import com.lzy.vo.AuditDetailVo;

import java.util.List;
public interface AuditDetailService extends IService<AuditDetail> {
    List<AuditDetailVo> findDetailsByApplyId(IPage<AuditDetailVo> page, Long applyId);

    int auditNotice(long userId);
}
