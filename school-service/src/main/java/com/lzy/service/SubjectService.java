package com.lzy.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.lzy.pojo.Subject;
import com.lzy.vo.SubjectVo;

import java.util.List;

/**
* @author Administrator
* @description 针对表【subject(学科表)】的数据库操作Service
* @createDate 2023-09-01 13:55:06
*/
public interface SubjectService extends IService<Subject> {

    List<Subject> selectSubjectVo(int current, int size, String name);

    //查询总数据量
    Long count(String name);


     List<Subject> listLikeSelectByName(String name) ;

    //根据课程id找到对应课程的评价信息
    List<SubjectVo> selectSubjectByUserId(Long facultiesId,Long subjectId);


}
