package com.lzy.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lzy.pojo.*;
import com.lzy.vo.*;
import com.lzy.util.PageUtils;

import java.util.Date;
import java.util.List;

/**
* @author zyj
* @description 针对表【user】的数据库操作Service
* @createDate 2023-08-29 15:54:15
*/

public interface TeacherService{
    //查询自己所有课程
    List<UserSubjectVo> allTeaSubject (Long teaId);
    //先查询所有选该课程的学生信息，再点名
    List<AttendanceVo> findAllStudentBySubId(Long subjectId);


    //根据时间和科目查询学生签到状态
//    String queryStuAttend(Date startTime, Long subjectId);

    //查询所有课程
    List<Subject> allSubject(PageUtils subjectVo);
    //添加课程
    String teaAddSubject(UserSubject userSubject);

    Long subCount(String name);

    int updateStudentAttendanceByStuId(Integer status, Long studentId,Long subjectId);

    List<Subject> allSubject();
    List<AttendanceVo> queryStuAttendInfo(Integer status,  Long facultiesId, Date startTime, Long subjectId);
    Long attCountByUserId(Long userId,Long subjectId);

    //教师评价
    String teaEvaTeaBySubId(Evaluate evaluate);

    List<Subject> listLikeSelectByName(String name) ;

    void classroomQuery(ClassroomPageVo classroomVo);

    List<Faculties> selectFac();

    List<EvaluateVo> findEvaBySubjectId(Long subjectId, Long roleId, Long facultiesId, Long userId,Long createBy);



    void selectStuBySub(StuPageVo stuPageVo);

    void selectStuScorePageByClassId(StuScorePageVo stuScorePageVo);

    List<ScoreInfoVo> selectScoreByFacAndUserId(Long subjectId, Long facultiesId);

    void updateIsScore(Long userId, Long subjectId);

}
