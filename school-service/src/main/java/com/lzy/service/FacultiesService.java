package com.lzy.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lzy.pojo.Faculties;


/**
* @author zyj
* @description 针对表【faculties(院系表)】的数据库操作Service
* @createDate 2023-09-12 13:50:27
*/
public interface FacultiesService extends IService<Faculties> {

}
