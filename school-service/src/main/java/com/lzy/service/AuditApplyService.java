package com.lzy.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.lzy.pojo.AuditApply;
import com.lzy.pojo.AuditDetail;
import com.lzy.vo.AuditApplyVo;
import org.springframework.stereotype.Service;

import java.util.List;
public interface AuditApplyService extends IService<AuditApply> {

    void apply(AuditApply auditApply) throws JsonProcessingException;

    List<AuditApply> findAuditList(long userId);

    void audit(AuditDetail detail) throws JsonProcessingException;

    List<AuditApplyVo> degree(IPage<AuditApplyVo> page, long userId);

    boolean cancel(long id);

    List<AuditApplyVo> degreeTwo(Long userId);

//    int applyNotice(long userId);
}
